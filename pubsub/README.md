###To build project
```
mvn package
```

### How to run PubSubSystem
```
cd pubsub
run EventManager.
run PubSubAgent.
when running EventManager, enter port you are going to use for PubSubAgent
when running PubSubAgent, enter 'localhost' for HOST IP if manager also running on local,
                          use same port with EventManager
```

### Run EventManager
```
java -cp target/pubsub-1.0.jar edu.rit.cs.pubsub.server.EventManager [PORT]

```

### Run PubSubAgent
```
java -cp target/pubsub-1.0.jar edu.rit.cs.pubsub.client.PubSubAgent [HOST IP] [PORT]

```
