package edu.rit.cs.pubsub.server;

import edu.rit.cs.pubsub.MessageQueue;
import edu.rit.cs.pubsub.PubSubMessage;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;

/**
 * Class responsible for handling all incoming requests to the
 * event server.
 *
 * @author Alexis Holler
 */
public class ConnectionsManager extends Thread implements ClientCallbackInterface {

    /* Open server socket listening for connections */
    private ServerSocket serverSocket;

    /* Determines if the server is currently accepting new activeConnections */
    private boolean acceptConnections = true;

    /* Map of currently connected client GUIDs and their processes */
    private Map<String, ClientConnection> activeConnections = new HashMap<>();

    /* Manages processing requests from all client connections */
    private DataManager manager;

    /* A single queue that handles performing update operations
      from client.
     */
    private MessageQueue messageQueue;

    /* A map that stores waiting messages for offline subscribers mapped to subscriber GUID */
    private Map<String, List<PubSubMessage>> messagesWaiting = new HashMap<>();

    /**
     * Constructor.
     *
     * @param serverSocket the server socket to accept activeConnections.
     */
    ConnectionsManager(ServerSocket serverSocket, DataManager manager) {
        this.serverSocket = serverSocket;
        this.manager = manager;
        this.messageQueue = new EventManagerMessageQueue(manager, this);
        this.start();
    }

    @Override
    public void run() {
        while (acceptConnections) {
            try {
                /* Accept an incoming connection */
                Socket clientSocket = serverSocket.accept();

                /* Spawn a new thread to handle accepting requests from the connection */
                new ClientConnection(clientSocket, this);

            } catch (SocketException e) {
                // Do nothing this is expected when socket closes
            } catch (IOException e) {
                System.out.println("IO: " + e.getMessage());
            }
        }
    }

    /**
     * Sends a broadcast message to all
     * connected clients.
     *
     * @param message the message to broadcast
     */
    void broadcast(PubSubMessage message) {
        for (String sGUID : activeConnections.keySet()) {
            sendMessage(sGUID, message);
        }
    }

    /**
     * Sends a message to an active client.
     *
     * @param sGUID   the GUID of the active client
     * @param message the message to send.
     */
    void sendMessage(String sGUID, PubSubMessage message) {
        if(sGUID == null){
            System.out.println("ERROR: Attempting to send a message to a null GUID.");
            return;
        }

        ClientConnection clientConnection = activeConnections.get(sGUID);
        if (clientConnection == null) {
            /* The client is offline add to message waiting list */
            System.out.println("Client " + sGUID + " is not online. Storing message.");

            /* Client must re-register if connection ends before sending
             * registration GUID
             */
            if (message.getType() != PubSubMessage.Type.REGISTER)
                addMessageWaiting(sGUID, message);
        } else {
            /* Client is online send the message */
            clientConnection.sendMessage(message);
        }
    }

    /**
     * Removes connection from list once it is finished.
     *
     * @param GUID the client GUID that was closed.
     */
    public synchronized void connectionClosed(String GUID) {
        if (GUID != null) {
            ClientConnection connection = activeConnections.get(GUID);
            if (connection != null)
                connection.stopConnection();
            activeConnections.remove(GUID);
            System.out.println("Connection closed with " + GUID);
        }
    }

    /**
     * Called when the client registers successfully.
     *
     * @param connection the active connection.
     * @param message    the register message to send containing the
     *                   new GUID
     */
    void clientRegistered(ClientConnection connection, PubSubMessage message) {
        String GUID = message.getGUID();
        if (connection != null) {
            /* Assign the GUID to the connection let the client know they've registered */
            activeConnections.put(GUID, connection);
            connection.assignClient(GUID);
            connection.sendMessage(message);

            /* If the client missed messages while offline, send them */
            List<PubSubMessage> messagesWaiting = getWaitingMessages(GUID);
            System.out.println("Sending {" + messagesWaiting.size() + "} messages waiting to GUID: " + GUID);
            for (PubSubMessage m : messagesWaiting) {
                connection.sendMessage(m);
            }
            clearMessagesWaiting(GUID);
        } else {
            System.out.println("ERROR: Connection does not exist for this registration unregistering.");
            /* Registration failed remove the registration from the list */
            manager.unregister(GUID);
        }
    }

    @Override
    public void clientMessageReceived(PubSubMessage message) {
        messageQueue.enqueue(message);
    }

    /**
     * Counts the number of active connections.
     *
     * @return number of active connections.
     */
    List<String> getConnections() {
        return new ArrayList<>(this.activeConnections.keySet());
    }

    /**
     * Getter.
     *
     * @return returns the server socket accepting connections.
     */
    ServerSocket getServerSocket() {
        return this.serverSocket;
    }

    /**
     * Stop accepting new activeConnections.
     */
    void close() {
        acceptConnections = false;
        messageQueue.stopProcessing();
        for (ClientConnection connection : activeConnections.values()) {
            connection.stopConnection();
        }
    }

    /**
     * Adds a waiting message to the internal list.
     *
     * @param sGUID   the offline subscriber GUID
     * @param message the message to send once they come online.
     */
    private void addMessageWaiting(String sGUID, PubSubMessage message) {
        List<PubSubMessage> messages = this.messagesWaiting.get(sGUID);
        if (messages == null) {
            List<PubSubMessage> messagesList = new ArrayList<>();
            messagesList.add(message);
            messagesWaiting.put(sGUID, messagesList);
        } else {
            messages.add(message);
            messagesWaiting.put(sGUID, messages);
        }
    }

    /**
     * Returns a list of messages the client missed while
     * they were offline
     *
     * @param sGUID the subscriber GUID that came online.
     * @return the list of missed messages.
     */
    List<PubSubMessage> getWaitingMessages(String sGUID) {
        List<PubSubMessage> messages = new ArrayList<>();
        /* Get the messages */
        List<PubSubMessage> queuedMessages = messagesWaiting.get(sGUID);

        if(queuedMessages != null)
            messages.addAll(queuedMessages);

        return messages;
    }

    void clearMessagesWaiting(String sGUID){
        messagesWaiting.remove(sGUID);
    }
}
