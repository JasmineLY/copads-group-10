package edu.rit.cs.pubsub.server;

import edu.rit.cs.pubsub.CommandLineInterface;
import edu.rit.cs.pubsub.PubSubMessage;
import edu.rit.cs.pubsub.Topic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A class that handles defining and processing command line
 * inputs for the EventManager.
 * @author Alexis Holler
 */
class EventManagerCommandLine extends CommandLineInterface {

    /**
     * Constructor.
     *
     * @param manager     the manager that holds information about internal data
     * @param connections the manager that holds information about connections.
     */
    EventManagerCommandLine(DataManager manager, ConnectionsManager connections) {
        super();
        addCommand("topics", 't', "Print the list of advertised topics",
                s -> print_topics(manager.getTopics()));

        addCommand("address", 'a', "Print the server ip",
                s -> System.out.println(connections.getServerSocket().getLocalSocketAddress()));

        addCommand("connections", 'c', "Print the list of active client connections",
                s -> print_clients(connections.getConnections()));

        addCommand("registrations", 'r', "Print the list of registered clients",
                s -> print_registrations(manager.getRegisteredClients()));

        addCommand("subscribers", 's', "Print the subscribers for the given topic",
                new String[]{"[TOPIC_ID|TOPIC_KEYWORD]"}, args -> print_subscribers(manager, args[0]));

        addCommand("keys", 'k', "Print the subscription keys for the given subscriber GUID",
                new String[]{"SUBSCRIBER_GUID"}, args -> print_keys(manager, args[0]));

        addCommand("messages", 'm', "Print the messages waiting for the given subscriber GUID",
                new String[]{"SUBSCRIBER_GUID"}, args -> print_messages_waiting(connections, args[0]));

        /* Print the final configuration */
        print_command_line_usage();
    }

    /**
     * Prints a formatted list of objects.
     *
     * WARNING: Array must not be null.
     *
     * @param objDescription a short description of the printed list
     * @param objects        the list of objects
     * @param ifEmptyDesc    what to print if the list is empty
     */
    private void print_list(String objDescription, List<Object> objects,
                            String ifEmptyDesc) {
        System.out.println(objDescription + " {" + objects.size() + "}:");
        if (objects.isEmpty())
            System.out.println("\t" + ifEmptyDesc);
        else {
            for (Object object : objects) {
                System.out.println("\t" + object);
            }
        }
    }

    /**
     * Prints the current list of advertised topics.
     *
     * @param topics the list of topics
     */
    private void print_topics(List<Topic> topics) {
        Collections.sort(topics);
        print_list("Topics",
                new ArrayList<>(topics),
                "No topics have been advertised.");
    }

    /**
     * Prints a list of currently connected clients.
     *
     * @param connectedClients the list of currently connected clients.
     */
    private void print_clients(List<String> connectedClients) {
        print_list("Active Connections",
                new ArrayList<>(connectedClients),
                "No clients currently connected.");
    }

    /**
     * Prints the list of registered clients.
     *
     * @param registrations the list of registrations.
     */
    private void print_registrations(List<String> registrations) {
        print_list("Registrations",
                new ArrayList<>(registrations),
                "No clients registered.");
    }

    /**
     * Prints all subscribers for the given topic.
     *
     * @param sub_key the subscriber key
     */
    private void print_subscribers(DataManager manager, String sub_key) {
        if (!manager.validateSubscribeKey(sub_key)) {
            System.out.println("\'" + sub_key + "\' is not a valid subscriber key.");
            return;
        }
        /* Lookup topic by name */
        String desc = sub_key;

        /* Lookup topic name for id */
        Topic t = manager.getTopic(sub_key);
        if (t != null)
            desc = t.getName();

        List<String> subscribers = manager.getSubscribers(sub_key);
        print_list("Subscribers for \"" + desc + "\"",
                new ArrayList<>(subscribers),
                "No subscribers.");
    }


    /**
     * Prints the list of subscription keys for the given
     * subscriber GUID.
     *
     * @param manager the manager that holds subscription information
     * @param sGUID the GUID of the subscriber
     */
    private void print_keys(DataManager manager, String sGUID) {
        if (!manager.getRegisteredClients().contains(sGUID)) {
            System.out.println("\'" + sGUID + "\' is not a registered client");
        } else {
            List<String> keys = manager.getSubscriptions(sGUID, true);
            print_list("Subscriptions for " + sGUID,
                    new ArrayList<>(keys),
                    "No subscriptions.");
        }
    }

    /**
     * Prints the list of messages waiting for the given sGUID
     *
     * @param connectionsManager the manager that stores connection information
     * @param sGUID the GUID of the subscriber
     */
    private void print_messages_waiting(ConnectionsManager connectionsManager, String sGUID){
        List<PubSubMessage> messages = connectionsManager.getWaitingMessages(sGUID);
        print_list("Messages waiting for " + sGUID,
                new ArrayList<>(messages),
                "No messages waiting.");
    }
}
