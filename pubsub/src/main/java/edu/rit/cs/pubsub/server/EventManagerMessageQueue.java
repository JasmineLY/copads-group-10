package edu.rit.cs.pubsub.server;

import edu.rit.cs.pubsub.*;

import java.util.List;

/**
 * MessageQueue that handles EventManager messages
 */
public class EventManagerMessageQueue extends MessageQueue {

    /* The DataManager to handle each request */
    private DataManager dataManager;

    /* The connections manager to handle sending messages to clients */
    private ConnectionsManager connectionsManager;

    /* A factory class to handle formatting PubSubMessages */
    private MessageFactory messageFactory;

    /**
     * Constructor.
     *
     * @param dataManager        a reference to the event dataManager that stores internal data.
     * @param connectionsManager a reference to the connectionsManager that stores connections.
     */
    EventManagerMessageQueue(DataManager dataManager, ConnectionsManager connectionsManager) {
        super();
        this.dataManager = dataManager;
        this.connectionsManager = connectionsManager;
        this.messageFactory = new MessageFactory();
        this.start();
    }

    /**
     * Processes EventManager messages.
     *
     * @param message the message to process
     * @throws ClassCastException if an unexpected object is sent.
     */
    protected void processMessage(PubSubMessage message) throws ClassCastException {
        switch (message.getType()) {
            case REGISTER:
                /* Register a client and associate it with the connection */
                ClientConnection connection = (ClientConnection) message.getObj();
                String clientGUID = message.getGUID();
                if (clientGUID == null) {
                    /* Generate a new GUID and assign it to the client */
                    String newGUID = dataManager.register();
                    connectionsManager.clientRegistered(connection, messageFactory.register(newGUID));
                } else {
                    if(dataManager.registrationExists(clientGUID))
                        /* Assign the new connection to the existing registration */
                        connectionsManager.clientRegistered(connection, messageFactory.register(clientGUID));
                    else
                        connection.sendMessage(messageFactory.error("\'" + clientGUID + "" +
                                "\' is not a valid GUID."));
                }
                break;
            case OFFLINE:
                /* Remove and close the connection */
                connectionsManager.connectionClosed(message.getGUID());
            case PUBLISH:
                /* Notify all subscribers of the publish */
                Event publish_event = (Event) message.getObj();
                /* Get the list of subscribers for the event */
                List<String> subscribers = dataManager.getSubscribers(publish_event);
                for (String sGUID : subscribers) {
                    /* Send a notify to each subscriber */
                    connectionsManager.sendMessage(sGUID,
                            messageFactory.notify(publish_event));
                }
                break;
            case ADVERTISE:
                /* A new topic has been advertised */
                Topic adv_topic = (Topic) message.getObj();
                Topic newTopic = dataManager.addTopic(adv_topic);

                /* Broadcast the new topic to all clients */
                connectionsManager.broadcast(messageFactory.broadcast(newTopic));
                break;
            case SUBSCRIBE:
                /* Subscribe to the given topic or keyword*/
                String subscribe_key = (String) message.getObj();
                String err = dataManager.addSubscriber(message.getGUID(), subscribe_key);
                if(err != null)
                    /* Failed to add subscriber send error message */
                    connectionsManager.sendMessage(message.getGUID(), messageFactory.error(err));
                break;
            case UNSUBSCRIBE:
                /* Unsubscribe from the given topic or keyword */
                String unsub_key = (String) message.getObj();
                dataManager.removeSubscriber(message.getGUID(), unsub_key);
                break;
            case REQUEST:
                /* Query the request data and send it to agent */
                PubSubMessage.RequestType request = (PubSubMessage.RequestType) message.getObj();
                switch (request) {
                    case SUBSCRIPTIONS:
                        /* Send list of all subscriptions for the given GUID */
                        String GUID = message.getGUID();
                        connectionsManager.sendMessage(message.getGUID(),
                                messageFactory.replySubscriptions(GUID, dataManager.getSubscriptions(GUID, false)));
                        break;
                    case TOPICS:
                        /* Send all advertised topics */
                        connectionsManager.sendMessage(message.getGUID(),
                                messageFactory.replyTopics(dataManager.getTopics()));
                        break;
                }
                break;
            default:
                System.out.println("Received unexpected message of type " + message.getType());
                break;
        }
    }
}
