package edu.rit.cs.pubsub;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representative of an Topic object
 * in PubSub System.
 *
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-09-27
 */
public class Topic implements Serializable, Comparable<Topic> {
    private int id;
    private List<String> keywords;
    private String name;

    /**
     * Topic's constructor
     * need at least a name and keywords to create  a topic
     *
     * @param keywords topic's keywords
     * @param name     topic's name
     */
    public Topic(String name, List<String> keywords) {
        this.name = name;
        this.keywords = new ArrayList<>();

        if (keywords != null) {
            this.keywords.addAll(keywords);
        }
    }

    /**
     * get the current topic's id
     *
     * @return topic's id
     */
    public int getId() {
        return id;
    }

    /**
     * get the current topic's name
     *
     * @return topic's name
     */
    public String getName() {
        return name;
    }

    /**
     * get the current topic\'s keywords
     *
     * @return the list of keywords for this topic
     */
    public List<String> getKeywords() {
        return keywords;
    }

    /**
     * set the id for this topic when server decide one
     *
     * @param id the unique id for this topic
     * @return this topic
     */
    public Topic setId(int id) {
        this.id = id;
        return this;
    }

    /**
     * Determines if the topic contains the
     * given keyword.
     *
     * @param keyword the keyword to check
     * @return true if it exists false
     * otherwise
     */
    public boolean hasKeyword(String keyword) {
        return keywords.contains(keyword);
    }

    /**
     * Formats the keywords as a comma separated
     * list
     *
     * @return a string representation of the keywords
     */
    public String getFormattedKeywords() {
        StringBuilder keywordFormatted = new StringBuilder();
        keywordFormatted.append("[");
        for (String keyword : keywords) {
            keywordFormatted.append(keyword);
            keywordFormatted.append(",");
        }

        if (!keywords.isEmpty())
            keywordFormatted.deleteCharAt(keywordFormatted.length() - 1);

        keywordFormatted.append("]");

        return keywordFormatted.toString();
    }

    /**
     * Returns a string representing the topic
     * to be printed to the user.
     * @return a string representation of the topic
     */
    public String getUserReadableString(){
        StringBuilder builder = new StringBuilder();
        builder.append("================ Topic ================\n");
        builder.append("Name: ").append(this.name).append("\n");
        builder.append("Keywords: ").append(this.getFormattedKeywords());

        return builder.toString();
    }

    @Override
    public String toString() {
        String format = "%d: %-5s %5s";
        return String.format(format, id, name, getFormattedKeywords());
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || o.getClass() != this.getClass()) {
            return false;
        }

        Topic topic = (Topic) o;

        return topic.id == this.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(Topic t) {
        return Integer.compare(this.getId(), t.getId());
    }
}
