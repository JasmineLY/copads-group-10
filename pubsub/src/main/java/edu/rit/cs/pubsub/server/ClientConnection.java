package edu.rit.cs.pubsub.server;

import edu.rit.cs.pubsub.Connection;
import edu.rit.cs.pubsub.PubSubMessage;

import java.net.Socket;

/**
 * Class representative of a single ongoing connection between a server
 * and a client.
 *
 * @author Alexis Holler
 */
public class ClientConnection extends Connection {

    /* Callback function to remove connection when closed. */
    private ClientCallbackInterface callback;

    /* The GUID of the client associated with this connection */
    private String GUID = null;

    /**
     * Constructor.
     *
     * @param clientSocket the client socket used to communicate.
     * @param callback     the function to call once the connection is closed by
     *                     the client.
     */
    ClientConnection(Socket clientSocket,
                     ClientCallbackInterface callback) {
        super(clientSocket);
        this.callback = callback;
        this.start();
    }

    /**
     * Once the client registers through the connection
     * we can assign a GUID
     *
     * @param GUID the client GUID
     */
    void assignClient(String GUID) {
        this.GUID = GUID;
    }

    /**
     * Called when the socket receives a PubSubMessage
     * @param clientMsg the message received
     * @return true if the socket should close as a result
     * false otherwise.
     */
    protected boolean receivedMessage(PubSubMessage clientMsg) {
        System.out.println("Server received message of type: " + clientMsg.getType());

        /* If the client went offline close the socket. */
        if(clientMsg.getType() == PubSubMessage.Type.OFFLINE)
            return true;

        /* If the client is registering associate the current thread as the object
         * in the message
         */
        if (clientMsg.getType() == PubSubMessage.Type.REGISTER)
            callback.clientMessageReceived(new PubSubMessage(clientMsg.getType(),
                    clientMsg.getGUID(), this));
        else
            callback.clientMessageReceived(clientMsg);

        return false;
    }

    protected void connectionClosed() {
        /* Called when client disconnects. */
        this.callback.connectionClosed(GUID);
    }
}
