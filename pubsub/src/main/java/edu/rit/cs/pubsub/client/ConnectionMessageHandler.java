package edu.rit.cs.pubsub.client;

import edu.rit.cs.pubsub.*;

import java.net.Socket;

/**
 * handle the messages to send/connect with EventManager
 *
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-09-28
 */
public class ConnectionMessageHandler extends Connection {

    private MessageQueue queue;

    /**
     * constructor for this class
     * get the socket to connect with EventManager
     *
     * @param socket the socket for current client
     */
    ConnectionMessageHandler(Socket socket, PubSubAgent agent) {
        super(socket);
        this.queue = new AgentMessageQueue(agent);
        this.start();
    }

    @Override
    protected boolean receivedMessage(PubSubMessage message) {
        queue.enqueue(message);
        return false;
    }

    @Override
    protected void connectionClosed() {
        System.out.println("\nConnection closed with EventManager.");
        this.queue.stopProcessing();
    }
}
