package edu.rit.cs.pubsub;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Represents an ongoing client-server connection
 * in the PubSubSystem
 *
 * @author Alexis Holler
 */
public abstract class Connection extends Thread {

    /* The socket associated with this connection */
    private Socket socket;
    private ObjectOutputStream out;

    /**
     * Constructor.
     *
     * @param socket the socket for the ongoing connection
     */
    public Connection(Socket socket) {
        this.socket = socket;
        this.setupOutputStream(socket);
    }

    protected abstract boolean receivedMessage(PubSubMessage message);

    protected abstract void connectionClosed();

    /**
     * Sets up the output stream for the connection.
     *
     * @param socket the socket to associate the output
     *               stream with.
     */
    private void setupOutputStream(Socket socket) {
        try {
            /* Setup the output stream */
            out = new ObjectOutputStream(socket.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends the given message to the active client.
     *
     * @param message the message to send.
     */
    public void sendMessage(PubSubMessage message) {
        if (message == null ) return;
        try {
            out.writeObject(message);
        } catch (Exception e) {
            System.out.println("\nERROR: Failed to send message: " + message + " EventManager is offline.");
        }
    }

    /**
     * Closes the socket to end the ongoing
     * connection.
     */
    public void stopConnection() {
        System.out.println("Stop Connection.");
        try {
            socket.close();
        } catch (Exception e) {
            System.out.println("Failed to close socket.");
        }
    }

    @Override
    public void run() {
        try {
            Object obj;
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

            /* Read in PubSubMessage objects */
            while ((obj = inputStream.readObject()) != null) {
                PubSubMessage msg = (PubSubMessage) obj;
                boolean exit = receivedMessage(msg);
                if (exit) break;
            }

            // Close streams
            socket.close();
            inputStream.close();
            out.close();
        } catch (Exception e) {
            // Do nothing expected when socket closes.
        } finally {
            connectionClosed();
        }
    }
}
