package edu.rit.cs.pubsub;

import java.util.ArrayList;
import java.util.List;

/**
 * A queue that handles processing incoming client requests.
 *
 * @author Alexis Holler
 */
public abstract class MessageQueue extends Thread {

    /* A queue to hold messages not yet processed */
    private List<PubSubMessage> queue = new ArrayList<>();

    /* Determines if the queue should run */
    private boolean processMessages = true;

    /**
     * Enqueues an operation.
     */
    public synchronized void enqueue(PubSubMessage message) {
        this.queue.add(message);
    }

    /**
     * Dequeues the first operation if one exists.
     *
     * @return the first message to process if the queue
     * is not empty. Null otherwise.
     */
    private synchronized PubSubMessage dequeue() {
        if (queue.isEmpty()) return null;
        else return this.queue.remove(0);
    }

    /**
     * Ends the message queue process.
     */
    public synchronized void stopProcessing() {
        processMessages = false;
    }

    /**
     * Method that handles processing a PubSubMessage
     *
     * @param message the message to process.
     * @throws ClassCastException if message contains an unexpected object.
     */
    protected abstract void processMessage(PubSubMessage message) throws ClassCastException;

    @Override
    public void run() {
        PubSubMessage message;
        while (processMessages) {
            if ((message = dequeue()) != null) {
                try {
                    processMessage(message);
                } catch (ClassCastException e) {
                    System.out.println("Invalid object sent with message type: " +
                            message.getType() + "; obj: " + message.getObj());
                }
            }
        }
    }
}
