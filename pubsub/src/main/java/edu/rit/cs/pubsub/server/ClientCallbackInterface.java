package edu.rit.cs.pubsub.server;

import edu.rit.cs.pubsub.PubSubMessage;

/**
 * A callback interface passed to a new connection
 * to notify when a connection has closed.
 *
 * @author Alexis Holler
 */
public interface ClientCallbackInterface {
    void connectionClosed(String GUID);

    void clientMessageReceived(PubSubMessage message);
}
