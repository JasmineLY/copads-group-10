package edu.rit.cs.pubsub.client;

import edu.rit.cs.pubsub.CommandLineInterface;
import edu.rit.cs.pubsub.Event;
import edu.rit.cs.pubsub.Topic;

import java.util.*;

/**
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-09-27
 */
public class PubSubAgentCommandLine extends CommandLineInterface {

    /*  the current agent active  */
    private PubSubAgent pubSubAgent;

    private boolean registered;

    /**
     * Constructor for commandline class
     * pass in agent to call functions
     *
     * @param agent the PubSubAgent
     */
    PubSubAgentCommandLine(PubSubAgent agent) {
        super();
        this.pubSubAgent = agent;
        addCommand("register", 'r',
                "Register as a new Agent with the EventManager", s -> {
                    agent.register();
                });
        addCommand("sign-in", 's', "Sign in as a returning Agent",
                new String[]{"GUID"}, s -> {
                    agent.re_register(s[0]);
                });
        /* Print the final configuration */
        print_command_line_usage();
    }

    @Override
    public boolean processLine(String line) {
        /* If we haven't registered show top level command line*/
        if (!registered)
            return super.processLine(line);
        else
            /* Otherwise show PubSubSystem options */
            start();

        return false;
    }

    /**
     * Tells the command line whether to show PubSubSystem
     * options.
     * @param registered whether the agent has registered or not
     */
    synchronized void setRegistered(boolean registered) {
        this.registered = registered;
        if (registered)
            System.out.println("Press ENTER to continue.");
    }

    /**
     * the commands for user to choose what to do
     */
    private synchronized void start() {
        Scanner in = new Scanner(System.in);
        //whether want to return to previous choice or not
        boolean regret = false;
        System.out.println("Welcome to the PubSubSystem.");
        while (!regret) {
            System.out.println("===========================================================");
            System.out.println("Make your choice: ");
            System.out.println("1: Advertise a new Topic");
            System.out.println("2: Publish an Event");
            System.out.println("3: Subscribe to a Topic or Keyword");
            System.out.println("4: Unsubscribe from a Topic or Keyword");
            System.out.println("5: View Subscriptions");
            System.out.println("6: View Topics");
            System.out.println("7: Leave the PubSubSystem");
            System.out.print("Your choice: ");
            String choice = in.nextLine().trim();
            switch (choice) {
                case "1":
                    pubSubAgent.advertise(createTopic(in));
                    break;
                case "2":
                    /* If there are no topics we cannot publish */
                    if (!pubSubAgent.getTopics().isEmpty())
                        pubSubAgent.publish(createEvent(in));
                    else
                        System.out.println("Cannot publish an event, no Topics have been advertised.");
                    break;
                case "3":
                    /* If there are no topics we cannot subscribe */
                    if (!pubSubAgent.getTopics().isEmpty())
                        subscriberChoices(in);
                    else
                        System.out.println("Cannot subscribe, no topics have been advertised.");
                    break;
                case "4":
                    /* If there are no subscriptions don't bother navigating to unsubscribe */
                    if(pubSubAgent.numSubscriptions() > 0)
                        unsubscribeChoices(in);
                    else
                        System.out.println("Cannot unsubscribe, you are not subscribed to any topics or keywords.");
                    break;
                case "5":
                    pubSubAgent.listSubscriptions();
                    break;
                case "6":
                    pubSubAgent.listTopics(pubSubAgent.getTopics());
                    break;
                case "7":
                    regret = true;
                    /* Navigate back up to main command line */
                    print_command_line_usage();
                    /* Disconnect the agent */
                    setRegistered(false);
                    pubSubAgent.offline();
                    break;
                default:
                    System.out.println("Invalid choice \'" + choice + "\'");
                    break;
            }
        }
    }

    /**
     * the choices for subscribers to unsubscribe
     *
     * @param in scanner
     */
    private synchronized void unsubscribeChoices(Scanner in) {
        //whether want to return to previous choice or not
        boolean regret = false;
        while (!regret) {
            System.out.println("===========================================================");
            System.out.println("Unsubscribe choices: ");
            System.out.println("1: View subscriptions");
            System.out.println("2: Unsubscribe from a Topic");
            System.out.println("3: Unsubscribe from a Keyword");
            System.out.println("4: Unsubscribe from all subscriptions");
            System.out.println("5: Return to previous choice");
            System.out.print("Enter your choice:");
            String choice = in.nextLine().trim();
            switch (choice) {
                case "1":
                    pubSubAgent.listSubscriptions();
                    break;
                case "2":
                    Topic topic;
                    /* Check if we are subscribed to any topics */
                    List<Topic> topicSubscriptions = pubSubAgent.getSubscribedTopics();
                    if(topicSubscriptions.isEmpty()) {
                        System.out.println("No topics to unsubscribe from.");
                        continue;
                    }

                    Collections.sort(topicSubscriptions);
                    /* Prompt the user to select a topic */
                    if ((topic = chooseTopic(in, topicSubscriptions)) != null) {
                        pubSubAgent.unsubscribe(topic);
                    }
                    break;
                case "3":
                    String keyword;
                    /* Check if we are subscribed to any keywords */
                    List<String> keywordSubscriptions = pubSubAgent.getSubscribedKeywords();
                    if(keywordSubscriptions.isEmpty()){
                        System.out.println("No keywords to unsubscribe from.");
                        continue;
                    }

                    Collections.sort(keywordSubscriptions);
                    /* Prompt the user to select a keyword */
                    if ((keyword = chooseKeyword(in, pubSubAgent.getSubscribedKeywords())) != null){
                        pubSubAgent.unsubscribe(keyword);
                    }
                    break;
                case "4":
                    pubSubAgent.unsubscribeAll();
                    System.out.println("Unsubscribed from all subscriptions.");
                    break;
                case "5":
                    System.out.println("Sending you back to previous choice.");
                    regret = true;
                    System.out.println("===========================================================");
                    break;
                default:
                    System.out.println("Invalid choice.");
                    break;
            }
        }
    }

    /**
     * the choices for subscribers
     *
     * @param in the scanner
     */
    private synchronized void subscriberChoices(Scanner in) {
        //whether want to return to previous choice or not
        boolean regret = false;
        while (!regret) {
            System.out.println("\n===========================================================");
            System.out.println("Subscriber's choices: ");
            System.out.println("1: View all Topics advertised by publishers");
            System.out.println("2: Subscribe to a Topic");
            System.out.println("3: Subscribe to a Keyword (will receive events for all topics with matching keywords)");
            System.out.println("4: Return to previous choice");
            System.out.print("Enter your choice:");
            String choice = in.nextLine().trim();
            Topic topic = null;
            switch (choice) {
                case "1":
                    pubSubAgent.listTopics(pubSubAgent.getTopics());
                    break;
                case "2":
                    /* Check that topics have been advertised. */
                    List<Topic> advertisedTopics = pubSubAgent.getTopics();
                    if(advertisedTopics.isEmpty()){
                        System.out.println("No advertised topics.");
                        continue;
                    }

                    Collections.sort(advertisedTopics);
                    /* Prompt the user to choose a topic */
                    if ((topic = chooseTopic(in, advertisedTopics)) != null) {
                        pubSubAgent.subscribe(topic);
                    }
                    break;
                case "3":
                    String keyword;
                    /* Check that keywords have been advertised. */
                    List<String> advertisedKeywords = pubSubAgent.getKeywords();
                    if (advertisedKeywords.isEmpty()) {
                        System.out.println("No advertised keywords.");
                        continue;
                    }

                    Collections.sort(advertisedKeywords);
                    /* Prompt the user to choose a keyword */
                    if((keyword = chooseKeyword(in, advertisedKeywords)) != null){
                        pubSubAgent.subscribe(keyword);
                    }
                    break;
                case "4":
                    System.out.println("Sending you back to previous choice.");
                    regret = true;
                    System.out.println("===========================================================");
                    break;
                default:
                    System.out.println("Invalid choice.");
                    break;
            }
        }
    }

    /**
     * create a new topic to advertise
     *
     * @param in the scanner
     * @return the new topic create
     */
    private Topic createTopic(Scanner in) {
        String name;
        Set<String> keywords = new HashSet<>();
        System.out.println("Creating a new topic (Press ENTER to exit)");
        System.out.println("===========================================================");
        System.out.print("Enter a topic name:");
        name = in.nextLine().trim();
        if (name.equals("")) {
            System.out.println("Exiting create a topic.");
            return null;
        }


        System.out.print("Enter keywords separated by commas:");
        String[] inputs = in.nextLine().split(",");
        for (String input : inputs) {
            input = input.trim();
            if (!input.equals(""))
                keywords.add(input);
        }

        Topic topic = new Topic(name, new ArrayList<>(keywords));
        System.out.println();
        System.out.println(topic.getUserReadableString());

        while (true) {
            System.out.print("Do you want to advertise this topic? [y/n]:");
            String createTopic = in.nextLine().trim();
            if (createTopic.equalsIgnoreCase("y")
                    | createTopic.equalsIgnoreCase("yes")) {
                newTopicString(topic);
                return topic;
            } else if (createTopic.equalsIgnoreCase("n")
                    | createTopic.equalsIgnoreCase("no")) {
                System.out.println("Topic discarded.");
                return null;
            } else {
                System.out.println("Invalid input \'" + createTopic + "\'");
            }
        }
    }

    /**
     * format for announcing new topic when created
     */
    public void newTopicString(Topic t) {
        System.out.println("Successfully created new Topic [" + t.getName() +
                "] with keywords: " + t.getFormattedKeywords());
    }

    /**
     * create new event to publish
     *
     * @param in the scanner
     * @return the event to publish
     */
    private Event createEvent(Scanner in) {
        System.out.println("Create a new event (Press ENTER to exit)");

        System.out.println("===========================================================");
        System.out.println("Choose the topic for the event.");
        Topic topic = chooseTopic(in, pubSubAgent.getTopics());
        if (topic == null) {
            System.out.println("Discarding event.");
            return null;
        }

        System.out.print("Enter an Event title:");
        String title = in.nextLine();
        if (title.equals("")) {
            System.out.println("Discarding event.");
            return null;
        }

        System.out.print("Enter the Event's content, enter a blank line when done:");
        StringBuilder content = new StringBuilder();
        String input = "";
        while (!((input = in.nextLine()).isEmpty())) {
            content.append(input);
        }
        Event event = new Event(topic, title, content.toString());
        System.out.println();
        System.out.println(topic.getUserReadableString());
        System.out.println(event.getUserReadableString());

        while (true) {
            System.out.print("Do you want to publish this event? [y/n]:");
            String createEvent = in.nextLine().trim();
            if (createEvent.equalsIgnoreCase("y")
                    | createEvent.equalsIgnoreCase("yes")) {
                newEventString(event);
                return event;
            } else if (createEvent.equalsIgnoreCase("n")
                    | createEvent.equalsIgnoreCase("no")) {
                System.out.println("Discarded event.");
                return null;
            } else {
                System.out.println("Invalid input \'" + createEvent + "\'");
            }
        }
    }

    /**
     * format for announcing new event when created
     */
    public void newEventString(Event e) {
        System.out.println("Successfully create new Event with topic [" + e.getTopic().getName()
                + "] and title [" + e.getTitle() + "]");
        System.out.println("Contents of this Event: {" + e.getContent() + "}");
    }

    /**
     * choose the topic to subscribe or make event
     *
     * @param in the scanner
     * @return the topic subscriber/publisher choose
     */
    private Topic chooseTopic(Scanner in, List<Topic> topics) {
        while (true) {
            System.out.println("===========================================================");
            pubSubAgent.listTopics(topics);
            System.out.print("Enter the ID of the topic you want to select (Press ENTER to quit):");
            String choice = in.nextLine().trim();
            if (choice.equals("")) {
                return null;
            }

            try {
                int topic_id = Integer.parseInt(choice);
                for (Topic t : topics) {
                    if (t.getId() == topic_id)
                        return t;
                }

                System.out.println("A topic with that ID doesn't exist.");
            } catch (NumberFormatException e) {
                System.out.println("Invalid choice \'" + choice + "\'");
            }
        }
    }

    /**
     * choose the keyword to subscribe to or unsubscribe from
     *
     * @param in the scanner
     * @return the keyword choosen
     */
    private String chooseKeyword(Scanner in, List<String> keywords) {
        while (true) {
            System.out.println("===========================================================");
            pubSubAgent.listKeywords(keywords);
            System.out.print("Enter the keyword you want to select (Press ENTER to quit):");
            String choice = in.nextLine().trim();
            if (choice.equals("")) {
                return null;
            }

            if(keywords.contains(choice))
                return choice;
            System.out.println("\'" + choice + "\' is not a valid keyword." );
        }
    }
}
