package edu.rit.cs.pubsub.server;

import edu.rit.cs.pubsub.Event;
import edu.rit.cs.pubsub.Topic;

import java.util.*;

/**
 * Handles storing and modifying topics
 * and events related to the event manager.
 *
 * @author Alexis Holler
 */
class DataManager {

    /* A mapping of subscription keys to a set of subscriber GUIDs */
    private HashMap<String, Set<String>> subscriberMap = new HashMap<>();

    /* A mapping of topic IDs to topic objects. */
    private Map<Integer, Topic> topicList = new HashMap<>();

    /* The set of registered client GUIDs */
    private Set<String> registeredClients = new HashSet<>();

    /**
     * Get all subscribers that need to be notified
     * of the given event.
     *
     * @param event the event to send.
     */
    List<String> getSubscribers(Event event) {
        if (event == null){
            System.out.println("Attempting to get subscribers for a null event.");
            return new ArrayList<>();
        }

        /* Lookup the topic */
        Topic topic = event.getTopic();
        /* Add the subscribers for that topic. A set is used to prevent duplicates */
        Set<String> subscribers = new HashSet<>(getSubscribers(String.valueOf(topic.getId())));
        for (String keyword : topic.getKeywords()) {
            /* Add all subscribers for each keyword */
            subscribers.addAll(getSubscribers(keyword));
        }

        return new ArrayList<>(subscribers);
    }

    /**
     * Returns the list of subscriptions for
     * the given subscriber GUID.
     *
     * @param sGUID the subscriber GUID
     * @return a list of subscription keys.
     */
    List<String> getSubscriptions(String sGUID, boolean userReadable) {
        List<String> subscriberList = new ArrayList<>();
        for (String key : subscriberMap.keySet()) {
            Set<String> subscribers = subscriberMap.get(key);
            if (subscribers.contains(sGUID)) {
                if (!userReadable)
                    subscriberList.add(key);
                else
                    subscriberList.add(getUserReadableKey(key));
            }
        }
        return subscriberList;
    }

    /**
     * Converts the subscription key into a
     * user readable format.
     *
     * @param key the subscription key
     * @return a user readable string representing
     * the key.
     */
    private String getUserReadableKey(String key) {
        Topic t = getTopic(key);
        if (t != null)
            return t.toString();

        /* Otherwise return the keyword */
        return key;
    }

    /**
     * Returns the topic for the given ID if
     * one exists.
     *
     * @param topicID the id of the topic
     * @return the topic if it exists null
     * otherwise.
     */
    Topic getTopic(String topicID) {
        try {
            int topic_id = Integer.parseInt(topicID);
            return topicList.get(topic_id);
        } catch (NumberFormatException e) {
            // Do nothing not a topic id
        }

        return null;
    }

    /**
     * Add a new topic when EventManager receives advertisement of new topic.
     *
     * @param topic the topic to add
     */
    Topic addTopic(Topic topic) {
        if (topicList.containsKey(topic.getId())){
            System.out.println("Topic " + topic + " already exists.");
            return null;
        } else {
            System.out.println("A new topic has been advertised: " + topic.toString());
            /* Set the id before adding it to the map */
            topic.setId(topicList.size() + 1);
            topicList.put(topic.getId(), topic);
            return topic;
        }
    }

    /**
     * Returns the list of advertised
     * topics stored in the EventManager
     *
     * @return a list of topics
     */
    List<Topic> getTopics() {
        return new ArrayList<>(topicList.values());
    }

    /**
     * Subscribes the given GUID to the given
     * topic key
     *
     * @param sGUID         the GUID of the subscriber
     * @param subscribe_key either a topicID or keyword
     * @return an error message if failed to add subscriber,
     * null otherwise.
     */
    String addSubscriber(String sGUID, String subscribe_key) {
        /* Check to make sure it is a valid key */
        if (validateSubscribeKey(subscribe_key)) {
            Set<String> subscribers = subscriberMap.get(subscribe_key);
            if (subscribers == null)
                subscribers = new HashSet<>();


            subscribers.add(sGUID);
            subscriberMap.put(subscribe_key, subscribers);
            return null;
        } else {
            String errMsg = "Subscribe key \'" + subscribe_key + "\' does not exist";
            System.out.println(errMsg);
            return errMsg;
        }
    }

    /**
     * Checks that the given subscribe key is
     * a valid topic id or keyword.
     *
     * @param subscribe_key the subscribe key to check
     * @return true if it is valid, false otherwise
     */
    boolean validateSubscribeKey(String subscribe_key) {
        if (getTopic(subscribe_key) != null)
            return true;
        else
            return !lookupKeyword(subscribe_key).isEmpty();
    }

    /**
     * Remove a subscriber from the internal
     * list.
     *
     * @param sGUID          the GUID of the subscriber to remove
     * @param subscriber_key the topic id or keyword to unsubscribe
     *                       from
     */
    void removeSubscriber(String sGUID, String subscriber_key) {
        if(sGUID == null) {
            System.out.println("Failed to remove subscriber key from null GUID.");
            return;
        }

        if(subscriber_key != null){
            Set<String> subscribers = subscriberMap.get(subscriber_key);
            subscribers.remove(sGUID);
            subscriberMap.put(subscriber_key, subscribers);
        } else {
            for(String sub_key : subscriberMap.keySet()){
                removeSubscriber(sGUID, sub_key);
            }
        }
    }

    /**
     * Given a keyword returns all topics
     * that contain that keyword.
     *
     * @param keyword a keyword for the topic
     * @return the list of topics that contain it
     */
    private List<Topic> lookupKeyword(String keyword) {
        List<Topic> keywordList = new ArrayList<>();
        for (Topic t : topicList.values()) {
            if (t.hasKeyword(keyword))
                keywordList.add(t);
        }
        return keywordList;
    }

    /**
     * Returns the list of subscribers for the given
     * subscriber key
     *
     * @param subscriber_key the topic id or keyword
     * @return the set of subscribers subscribed to
     * that key.
     */
    List<String> getSubscribers(String subscriber_key) {
        List<String> subscribers = new ArrayList<>();
        Set<String> sMap = subscriberMap.get(subscriber_key);
        if (sMap != null)
            subscribers.addAll(sMap);
        return subscribers;
    }

    /**
     * Gets the list of clients registered with the
     * EventManager.
     *
     * @return a list of client GUIDs
     */
    List<String> getRegisteredClients() {
        return new ArrayList<>(this.registeredClients);
    }

    /**
     * Determines if the given registration exists.
     * @param GUID the GUID to check
     * @return true if existing registration false
     * otherwise.
     */
    boolean registrationExists(String GUID){
        return this.registeredClients.contains(GUID);
    }

    /**
     * Unregisters a client.
     *
     * @param GUID the client GUID to unregister.
     */
    void unregister(String GUID) {
        registeredClients.remove(GUID);
    }

    /**
     * Registers a new client with the
     * EventManager.
     *
     * @return the GUID generated for the
     * client.
     */
    String register() {
        String GUID = UUID.randomUUID().toString();
        registeredClients.add(GUID);
        return GUID;
    }
}
