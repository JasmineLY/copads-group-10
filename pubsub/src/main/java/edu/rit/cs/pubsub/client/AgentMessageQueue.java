package edu.rit.cs.pubsub.client;

import edu.rit.cs.pubsub.Event;
import edu.rit.cs.pubsub.MessageQueue;
import edu.rit.cs.pubsub.PubSubMessage;
import edu.rit.cs.pubsub.Topic;

import java.util.List;
import java.util.Set;

/**
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-10-12
 */
public class AgentMessageQueue extends MessageQueue {

    private PubSubAgent agent;

    /**
     * the constructor for AgentMessageQueue
     * get the agent that create this queue
     * and use this agent to call functions
     * start the thread when create
     *
     * @param agent the agent for this message queue
     */
    AgentMessageQueue(PubSubAgent agent) {
        super();
        this.agent = agent;
        this.start();
    }

    @Override
    protected void processMessage(PubSubMessage message) throws ClassCastException {
        switch (message.getType()) {
            case REGISTER:
                String GUID = message.getGUID();
                System.out.println("Successfully registered with the EventManager, your GUID is: " + GUID);
                agent.setId(GUID);
                break;
            case NOTIFY:
                Event event = (Event) message.getObj();
                System.out.println("\n++++++++++++++++++++ NEW EVENT! ++++++++++++++++++++++");
                System.out.println("Your subscription has published a new event:\n" +
                        event.getUserReadableString() + "\n" +
                        event.getTopic().getUserReadableString());
                System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                break;
            case REPLY:
                List obj = (List) message.getObj();
                // get the list of topics current subscriber subscribed
                if (message.getGUID() != null) {
                    agent.setSubscribed(obj);
                } else {
                    // get all topics been advertised
                    agent.setTopics(obj);
                }
                break;
            case BROADCAST:
                Topic newTopic = (Topic) message.getObj();
                agent.updateTopic(newTopic);
                System.out.println("\n------------------- NEW TOPIC! -----------------------");
                System.out.println(newTopic.getUserReadableString());
                System.out.println("--------------------------------------------------------");
                break;
            case ERROR:
                String errorMsg = (String) message.getObj();
                System.out.println("\nError: " + errorMsg);
                break;
            default:
                System.out.println("Received unexpected message of type " + message.getType());
                break;
        }
    }
}
