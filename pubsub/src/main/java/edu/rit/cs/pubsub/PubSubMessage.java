package edu.rit.cs.pubsub;

import java.io.Serializable;

/**
 * A class representing a message sent between
 * the PubSubAgent and the EventManager.
 *
 * @author Alexis Holler
 */
public class PubSubMessage implements Serializable {

    public enum RequestType {SUBSCRIPTIONS, TOPICS}

    public enum Type {
        REGISTER, PUBLISH, SUBSCRIBE, UNSUBSCRIBE, ADVERTISE,
        NOTIFY, BROADCAST, OFFLINE, REPLY, REQUEST, ERROR
    }

    private Type type;

    private String GUID;

    private Object obj;

    /**
     * Constructor.
     *
     * @param type the type of message to send
     * @param obj  the object associated with that message
     *             either Topic or Event.
     */
    public PubSubMessage(Type type, String GUID, Object obj) {
        this.type = type;
        this.GUID = GUID;
        this.obj = obj;
    }

    /**
     * Getter.
     *
     * @return returns the message type.
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Getter.
     *
     * @return returns the object associated with
     * the message.
     */
    public Object getObj() {
        return this.obj;
    }

    /**
     * Getter.
     *
     * @return returns the GUID associated with the
     * client sending the message.
     */
    public String getGUID() {
        return this.GUID;
    }

    @Override
    public String toString() {
        String format = "%s";
        return String.format(format, this.type.toString());
    }
}
