package edu.rit.cs.pubsub;

import java.io.Serializable;

/**
 * Class representative of an Event object
 * in PubSub System.
 *
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-09-27
 */
public class Event implements Serializable {
    private int id;
    private Topic topic;
    private String title;
    private String content;

    public Event(Topic topic, String title, String content) {
        this.topic = topic;
        this.title = title;
        this.content = content;
    }

    /**
     * set the if for this event
     *
     * @param id event's unique id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * get the current event's id
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * get the current event's topic
     *
     * @return the topic
     */
    public Topic getTopic() {
        return topic;
    }

    /**
     * get the current event's title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * get the current event's content
     *
     * @return the content
     */
    public String getContent() {
        return content;
    }


    /**
     * Returns a string representing the event
     * to be printed to the user.
     * @return a string representation of the event
     */
    public String getUserReadableString(){
        StringBuilder builder = new StringBuilder();
        builder.append("================ Event ================\n");
        builder.append("Title: ").append(this.title).append("\n");
        builder.append("Content: ").append(this.content);

        return builder.toString();
    }

    @Override
    public String toString() {
        String format = "%d: %s %s %s";
        return String.format(format, id, topic.toString(), title, content);
    }
}
