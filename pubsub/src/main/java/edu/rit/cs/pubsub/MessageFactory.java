package edu.rit.cs.pubsub;

/**
 * Class that enforces message object type and simplifies
 * PubSubMessage creation.
 */
public class MessageFactory {

    private String GUID;

    public MessageFactory() {
    }

    /**
     * Set the GUID to be used to
     * generate all future messages.
     *
     * @param GUID the new GUID
     */
    public void setGUID(String GUID) {
        this.GUID = GUID;
    }

    /**
     * Generates a broadcast message to broadcast
     * a topic that has been advertised to all
     * clients.
     *
     * @param topic the topic to broadcast.
     * @return a broadcast PubSubMessage
     */
    public PubSubMessage broadcast(Topic topic) {
        return new PubSubMessage(PubSubMessage.Type.BROADCAST, this.GUID, topic);
    }

    /**
     * Generates a notify message to notify
     * subscribers of the given event.
     *
     * @param event the event to notify
     * @return a notify PubSubMessage
     */
    public PubSubMessage notify(Event event) {
        return new PubSubMessage(PubSubMessage.Type.NOTIFY, this.GUID, event);
    }

    /**
     * Generates a reply message to a
     * received subscriptions request.
     *
     * @param obj the object to send as a reply
     * @return a reply PubSubMessage
     */
    public PubSubMessage replySubscriptions(String sGUID, Object obj) {
        return new PubSubMessage(PubSubMessage.Type.REPLY, sGUID, obj);
    }

    /**
     * Generates a reply message to a
     * received topics request.
     *
     * @param obj the object to send as a reply
     * @return a reply PubSubMessage
     */
    public PubSubMessage replyTopics(Object obj) {
        return new PubSubMessage(PubSubMessage.Type.REPLY, this.GUID, obj);
    }

    /**
     * Generates a request message to get list of
     * subscribed topics
     *
     * @return a request PubSubMessage
     */
    public PubSubMessage requestSubscriptions() {
        return new PubSubMessage(PubSubMessage.Type.REQUEST, this.GUID, PubSubMessage.RequestType.SUBSCRIPTIONS);
    }

    /**
     * Generates a request message to get list of
     * topics from the server.
     *
     * @return a request PubSubMessage
     */
    public PubSubMessage requestTopics() {
        return new PubSubMessage(PubSubMessage.Type.REQUEST, this.GUID, PubSubMessage.RequestType.TOPICS);
    }

    /**
     * Generates a subscribe message for the given
     * topic_id or keyword.
     *
     * @param obj the topic_id or keyword to subscribe to
     * @return a subscribe PubSubMessage
     */
    public PubSubMessage subscribe(String obj) {
        return new PubSubMessage(PubSubMessage.Type.SUBSCRIBE, this.GUID, obj);
    }

    /**
     * Generates a PubSubMessage to unsubscribe from
     * the given topic_id or keyword.
     *
     * @param obj the topic_id or keyword to unsubscribe from
     * @return an unsubscribe PubSubMessage
     */
    public PubSubMessage unsubscribe(String obj) {
        return new PubSubMessage(PubSubMessage.Type.UNSUBSCRIBE, this.GUID, obj);
    }

    /**
     * Generates a PubSubMessage to unsubscribe the
     * client from all subscriptions.
     *
     * @return an unsubscribe PubSubMessage
     */
    public PubSubMessage unsubscribeAll() {
        return new PubSubMessage(PubSubMessage.Type.UNSUBSCRIBE, this.GUID, null);
    }

    /**
     * Generates an advertise message for the given
     * topic.
     *
     * @param topic the topic to advertise.
     * @return an advertise PubSubMessage
     */
    public PubSubMessage advertise(Topic topic) {
        return new PubSubMessage(PubSubMessage.Type.ADVERTISE, this.GUID, topic);
    }

    /**
     * Generates a publish message for the given
     * event.
     *
     * @param event the event to publish
     * @return a publish PubSubMessage
     */
    public PubSubMessage publish(Event event) {
        return new PubSubMessage(PubSubMessage.Type.PUBLISH, this.GUID, event);
    }

    /**
     * Generates a register message with the given GUID
     * as the message GUID
     *
     * @param GUID the GUID to register with.
     * @return a register PubSubMessage
     */
    public PubSubMessage register(String GUID) {
        return new PubSubMessage(PubSubMessage.Type.REGISTER, GUID, null);
    }

    /**
     * Generates a message to register with the
     * EventManager when the client has not previously
     * registered.
     *
     * @return a PubSubMessage to register the client to
     * the EventManager.
     */
    public PubSubMessage register() {
        if (this.GUID == null) {
            return new PubSubMessage(PubSubMessage.Type.REGISTER, null, null);
        } else {
            System.out.println("You already registered, your GUID is: " + GUID);
            return null;
        }
    }

    /**
     * Generates a message to tell the
     * the EventManager that the client
     * is going offline.
     *
     * @return an offline PubSubMessage
     */
    public PubSubMessage offline() {
        return new PubSubMessage(PubSubMessage.Type.OFFLINE, this.GUID, null);
    }

    /**
     * Generates an error PubSubMessage
     * @param errMsg the error to return
     * @return an error PubSubMessage
     */
    public PubSubMessage error(String errMsg){
        return new PubSubMessage(PubSubMessage.Type.ERROR, this.GUID, errMsg);
    }
}
