package edu.rit.cs.pubsub.server;


import edu.rit.cs.pubsub.CommandLineInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;

/**
 * Class representative of an EventManager in a
 * PubSub System. Sends, receives and processes
 * client requests.
 *
 * @author Alexis Holler
 */
public class EventManager {

    /**
     * Start the service and begin
     * accepting subscription/publication events.
     *
     * @param onPort the port to open for connections.
     */
    private void startService(int onPort) {
        try {
            /* Open the socket for clients to connect */
            ServerSocket listenSocket = new ServerSocket(onPort);
            System.out.println("EventManager is online and ready for clients on port " + onPort);

            /* Read from stdin */
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String line;

            /* Initialize data */
            DataManager dataManager = new DataManager();

            /* Start another thread to accept incoming connections */
            ConnectionsManager connections = new ConnectionsManager(listenSocket, dataManager);

            /* Create object to process command line input. */
            CommandLineInterface commandLine = new EventManagerCommandLine(dataManager, connections);

            /* Start reading command line input */
            while ((line = reader.readLine()) != null) {
                boolean exit = commandLine.processLine(line);
                if (exit) break;
            }

            /* Stop accepting connections */
            connections.close();
            System.out.println("Closing port " + onPort);
            /* Close the socket when finished */
            listenSocket.close();
            System.out.println("EventManager is offline.");
        } catch (IOException e) {
            System.out.println("IO Error:" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Prints the correct usage when running
     * Event Manager through command line.
     */
    private static void print_usage() {
        System.out.println("Usage:");
        System.out.println("java [OPTIONS] edu.rit.cs.pubsub.server.EventManager [PORT]");
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            print_usage();
            return;
        }
        int portNumber = Integer.parseInt(args[0]);
        new EventManager().startService(portNumber);
    }
}
