package edu.rit.cs.pubsub.client;

import edu.rit.cs.pubsub.Topic;

public interface Subscriber {
    /*
     * subscribe to a topic
     */
    public void subscribe(Topic topic);

    /*
     * subscribe to a topic with matching keywords
     */
    public void subscribe(String keyword);

    /*
     * unsubscribe from a topic
     */
    public void unsubscribe(Topic topic);

    /*
     * unsubscribe from a keyword
     */
    public void unsubscribe(String keyword);

    /*
     * unsubscribe to all subscribed topics
     */
    public void unsubscribeAll();

    /*
     * show the list of topics current subscribed to
     */
    public void listSubscriptions();

}
