package edu.rit.cs.pubsub.client;


import edu.rit.cs.pubsub.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.*;

/**
 * Class representative of Publishers and Subscribers
 * in a PubSub System.
 *
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-09-27
 */
public class PubSubAgent implements Publisher, Subscriber {

    /* Topics advertised by all publishers */
    private Map<Integer, Topic> topics = new HashMap<>();
    /* Subscriber keys subscribed to by current agent */
    private Set<String> subscribed = new HashSet<>();

    /* Connection handler for event manager connection */
    private ConnectionMessageHandler handler;

    /* A factory to generate PubSubMessages */
    private MessageFactory messageFactory = new MessageFactory();

    /* A reference to the command line interface */
    private PubSubAgentCommandLine commandLine;

    private int serverPort;
    private String serverIP;

    /**
     * give the initial command interface to user
     * to start the Agent side interaction
     *
     * @param port the EventManager port to connect to
     * @param ip   the IP address of the event manager
     */
    private void startAgent(int port, String ip) {
        this.serverPort = port;
        this.serverIP = ip;
        try {
            /* Read from stdin */
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String line;

            /* Create object to process command line input. */
            commandLine = new PubSubAgentCommandLine(this);

            /* Start reading command line input */
            while ((line = reader.readLine()) != null) {
                boolean exit = commandLine.processLine(line);
                if (exit) break;
            }

        } catch (IOException e) {
            System.out.println("IO Error:" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * ask the handler to send register message
     */
    void register() {
        try {
            this.handler = new ConnectionMessageHandler(new Socket(serverIP, serverPort), this);
            handler.sendMessage(messageFactory.register());
        } catch (Exception e){
            System.out.println("ERROR: Unable to register: The EventManager is not online.");
        }
    }

    /**
     * ask the handler to send offline message
     */
    void offline() {
        handler.sendMessage(messageFactory.offline());
        handler.stopConnection();
    }

    /**
     * set the id to save it for further usage
     *
     * @param GUID the id for this agent
     */
    void setId(String GUID) {
        messageFactory.setGUID(GUID);
        commandLine.setRegistered(true);
        handler.sendMessage(messageFactory.requestTopics());
        handler.sendMessage(messageFactory.requestSubscriptions());
    }

    /**
     * ask the handler to re-register this agent
     * get the topics subscribed before and all topics advertised
     *
     * @param GUID the id for this agent to re-register
     */
    void re_register(String GUID) {
        try {
            this.handler = new ConnectionMessageHandler(new Socket(serverIP, serverPort), this);
            handler.sendMessage(messageFactory.register(GUID));
        } catch ( Exception e ){
            System.out.println("ERROR: Unable to sign-in: The EventManager is not online.");
        }
    }

    /**
     * get all topics advertised and save
     *
     * @param topics all topics advertised by publishers
     */
    void setTopics(List<Topic> topics) {
        this.topics.clear();
        for( Topic t : topics){
            this.topics.put(t.getId(), t);
        }
    }

    /**
     * make a copy of all the topics subscribed before
     *
     * @param subscribed the topics subscribed before offline
     */
    void setSubscribed(List<String> subscribed) {
        this.subscribed.clear();
        this.subscribed.addAll(subscribed);
    }

    /**
     * add the newTopic subscribed to the ;ist
     *
     * @param topic the new topic
     */
    void updateTopic(Topic topic) {
        if (!topics.values().contains(topic)) {
            this.topics.put(topic.getId(), topic);
        }
    }

    /**
     * get the list of all advertised topics
     *
     * @return the List of topics advertised by publishers
     */
    List<Topic> getTopics() {
        return new ArrayList<>(topics.values());
    }

    List<String> getKeywords() {
        Set<String> keywords = new HashSet<>();
        topics.values().forEach(topic -> keywords.addAll(topic.getKeywords()));
        return new ArrayList<>(keywords);
    }

    /**
     * show the list of topics current subscribed to
     */
    void listTopics(List<Topic> topics) {
        System.out.println("Topics: ");
        if (topics.size() == 0)
            System.out.println("\t No topics.");
        else {
            topics.forEach(topic -> System.out.println("\t" + topic.toString()));
        }
    }

    /**
     * Prints formatted list of given keywords
     * @param keywords the list of keywords to print
     */
    void listKeywords(List<String> keywords){
        System.out.println("Keywords: ");
        if(keywords.size() == 0)
            System.out.println("\t No keywords.");
        else {
            keywords.forEach(keyword -> System.out.println("\t" + keyword));
        }
    }

    /**
     * subscribe to a topic
     *
     * @param topic the topic to subscribe
     */
    @Override
    public void subscribe(Topic topic) {
        System.out.println("Subscribing to " + topic);
        new Thread(() -> {
            subscribed.add(String.valueOf(topic.getId()));
            handler.sendMessage(messageFactory.subscribe(String.valueOf(topic.getId())));
        }).start();
    }

    /**
     * subscribe to a topic with matching keywords
     *
     * @param keyword the keyword to subscribe
     */
    @Override
    public void subscribe(String keyword) {
        System.out.println("Subscribing to keyword \'" + keyword + "\'");
        new Thread(() -> {
            handler.sendMessage(messageFactory.subscribe(keyword));
            handler.sendMessage(messageFactory.requestSubscriptions());
        }).start();
    }

    /**
     * unsubscribe from a topic
     *
     * @param topic the topic to unsubscribe
     */
    @Override
    public void unsubscribe(Topic topic) {
        new Thread(() -> {
            subscribed.remove(String.valueOf(topic.getId()));
            handler.sendMessage(messageFactory.unsubscribe(String.valueOf(topic.getId())));
        }).start();
    }

    @Override
    public void unsubscribe(String keyword) {
        new Thread(() -> {
            subscribed.remove(keyword);
            handler.sendMessage(messageFactory.unsubscribe(keyword));
        }).start();
    }

    /**
     * unsubscribe to all subscribed topics
     */
    @Override
    public void unsubscribeAll() {
        new Thread(() -> {
            handler.sendMessage(messageFactory.unsubscribeAll());
            subscribed.clear();
        }).start();
    }

    /**
     * show the list of topics current subscribed to
     */
    @Override
    public void listSubscriptions() {
        System.out.println("Subscriptions:");
        listTopics(getSubscribedTopics());
        listKeywords(getSubscribedKeywords());
    }

    /**
     * Returns the number of subscriptions
     * @return the number of active subscriptions
     */
    int numSubscriptions(){
        return subscribed.size();
    }

    /**
     * Returns the list of subscribed topics.
     * @return a list of topics.
     */
    List<Topic> getSubscribedTopics(){
        List<Topic> subscribedTopics = new ArrayList<>();
        subscribed.forEach(s -> {
            try {
                int topicID = Integer.valueOf(s);
                Topic t = topics.get(topicID);
                if(t != null)
                    subscribedTopics.add(t);
            } catch (NumberFormatException e){ /* Do nothing */ }
        });
        return subscribedTopics;
    }

    /**
     * Returns a list of subscribed keywords.
     * @return a list of keywords.
     */
    List<String> getSubscribedKeywords(){
        List<String> keywords = new ArrayList<>();
        subscribed.forEach(s -> {
            try {
                Integer.valueOf(s);
            } catch (NumberFormatException e){
                keywords.add(s);
            }
        });
        return keywords;
    }

    /**
     * publish an event of a specific topic with title and content
     *
     * @param event the event to publish
     */
    @Override
    public void publish(Event event) {
        new Thread(() -> {
            if (event != null) {
                handler.sendMessage(messageFactory.publish(event));
            }
        }).start();
    }

    /**
     * advertise new topic
     *
     * @param newTopic the new topic to advertise
     */
    @Override
    public void advertise(Topic newTopic) {
        new Thread(() -> {
            if (newTopic != null) {
                handler.sendMessage(messageFactory.advertise(newTopic));
            }
        }).start();
    }

    /**
     * the usage for the class
     */
    private static void print_usage() {
        System.out.println("Usage:");
        System.out.println("java [OPTIONS] edu.rit.cs.pubsub.server.PubSubAgent [IP] [PORT]");
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            print_usage();
            return;
        }
        String ip = args[0];
        int portNumber = Integer.parseInt(args[1]);
        new PubSubAgent().startAgent(portNumber, ip);
    }

}
