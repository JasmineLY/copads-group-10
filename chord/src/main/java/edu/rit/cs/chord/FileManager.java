package edu.rit.cs.chord;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class FileManager {

    /* The default directory to download and upload files from
     * The current directory should return 'copads-group-10'
     */
    protected static String DEFAULT_FILE_DIRECTORY
            = System.getProperty("user.dir") + "\\chord\\src\\main\\files\\";

    private static String DELETE_ON_EXIT_KEY = "delete_on_exit";

    protected String downloadDirectory;
    private String uploadDirectory;

    private boolean deleteOnExit = false;

    protected FileManager(Config config){
        int deleteFiles = config.getIntegerConfig(DELETE_ON_EXIT_KEY);
        if(deleteFiles == 1)
            deleteOnExit = true;
    }

    protected void setDownloadDirectory(String downloadDirectory) {
        if (downloadDirectory.startsWith("."))
            this.downloadDirectory = System.getProperty("user.dir") + downloadDirectory.substring(1);
        else
            this.downloadDirectory = downloadDirectory;
    }

    protected void setUploadDirectory(String uploadDirectory) {
        if (uploadDirectory.startsWith("."))
            this.uploadDirectory = System.getProperty("user.dir") + uploadDirectory.substring(1);
        else
            this.uploadDirectory = uploadDirectory;
    }

    /**
     * Returns the download directory to
     * download all files to.
     *
     * @return the directory path
     */
    public String getDownloadFileDirectory() {
        String dir = DEFAULT_FILE_DIRECTORY;
        if (this.downloadDirectory != null)
            dir = this.downloadDirectory;

        return dir;
    }

    /**
     * Returns the upload directory to
     * upload all files to.
     *
     * @return the directory path
     */
    public String getUploadFileDirectory() {
        if (this.uploadDirectory == null)
            return DEFAULT_FILE_DIRECTORY;
        else
            return this.uploadDirectory;
    }

    /**
     * Create the appropriate download directory
     * before creating the file in the directory.
     *
     * @return true if success, false otherwise.
     */
    private boolean createDownloadDirectory() {
        File downloadDirectory = new File(getDownloadFileDirectory());
        if(deleteOnExit)
            downloadDirectory.deleteOnExit();
        /* Create directories before creating file */
        if (!downloadDirectory.exists() & !downloadDirectory.mkdirs()) {
            System.out.println("Failed to make the necessary directories to store file.");
            return false;
        }
        return true;
    }

    /**
     * download the file onto the system
     *
     * @param downloadFile the file to download
     * @return true if the file was stored successfully
     * false otherwise
     */
    public boolean storeFile(File downloadFile) {
        try {
            if (!createDownloadDirectory())
                return false;

            /* Save the file in our local storage */
            File newFile = new File(generateFilePath(getDownloadFileDirectory(), downloadFile.getName()));
            /* Remove when the program quits to prevent constantly deleting between runs */
            if(deleteOnExit)
                newFile.deleteOnExit();
            /* Create the file */
            if (!newFile.createNewFile())
                System.out.println("File \'" + newFile.getName() + "\' already exists!");

            writeContent(downloadFile, newFile);
            return true;
        } catch (Exception e) {
            System.out.println("Failed to download the file \'"
                    + downloadFile.getName() + "\': " + e.getMessage());
        }
        return false;
    }

    /**
     * Hashes the given filename.
     *
     * @param filename the filename to hash.
     * @return a hash value as an integer.
     */
    public static int hashFilename(String filename) {
        return filename.hashCode();
    }

    /**
     * Given a directory generates the path if the given filename
     * was in the given directory.
     *
     * @param directory the directory
     * @param filename  the filename
     * @return the concatenation of the two in the proper format.
     */
    public static String generateFilePath(String directory, String filename) {
        return directory + "\\" + filename;
    }

    /**
     * Returns the filenames stored on the current
     * node.
     *
     * @return the list of filenames.
     */
    public List<String> getLocalFilenames() {
        List<String> localFilenames = new ArrayList<>();
        getLocalFiles().forEach((file) -> localFilenames.add(file.getName()));
        return localFilenames;
    }

    /**
     * Returns a list of locally stored
     * files.
     *
     * @return a list of files
     */
    public List<File> getLocalFiles() {
        List<File> localFiles = new ArrayList<>();
        File folder = new File(getDownloadFileDirectory());
        if(deleteOnExit)
            folder.deleteOnExit();
        if (folder.exists()) {
            File[] files = folder.listFiles();
            if (files != null)
                localFiles.addAll(Arrays.asList(files));
        }
        return localFiles;
    }

    /**
     * write the content to download file
     *
     * @param oldFile the file we get from other peer
     * @param newFile the file we "download"
     */
    private void writeContent(File oldFile, File newFile) {
        try {
            FileReader fr = new FileReader(oldFile);
            FileWriter fw = new FileWriter(newFile);
            int ch;
            while ((ch = fr.read()) != -1) {
                fw.append((char) ch);
            }
            fr.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public abstract void uploadFile(File file);

    public abstract void downloadFile(String filename);

    public abstract List<String> getAllFilenames();
}
