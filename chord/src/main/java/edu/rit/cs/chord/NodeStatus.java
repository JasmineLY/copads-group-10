package edu.rit.cs.chord;

/**
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-11-09
 */
public enum NodeStatus {
    ONLINE, OFFLINE
}
