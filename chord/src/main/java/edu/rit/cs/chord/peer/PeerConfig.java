package edu.rit.cs.chord.peer;

import edu.rit.cs.chord.Config;

/**
 * Handles parsing and defining keys in the Peer config file.
 *
 * @author Alexis Holler
 */
class PeerConfig extends Config {

    private static String CONFIG_FILE_PATH
            = System.getProperty("user.dir") + "\\src\\main\\java\\edu\\rit\\cs\\chord\\peer\\peer.cfg";


    /* Config keys */
    static final String SERVER_PORT_KEY = "server_port";
    static final String DOWNLOAD_DIR_KEY = "download_directory";
    static final String N_KEY = "N";

    PeerConfig() {
        super(CONFIG_FILE_PATH);
    }
}
