package edu.rit.cs.chord.peer;

import edu.rit.cs.chord.NodeID;

import java.util.HashMap;
import java.util.Map;

/**
 * Class that represents the finger table
 * of a single node in a DHT system.
 */
class FingerTable {

    /* The ID of the node holding the finger table */
    private Integer nodeID;

    /* The size of the finger table */
    private int table_size;

    /* The total number of nodes */
    private int numPeers;

    /* The table that holds all the entries */
    private Map<Integer, NodeID> table = new HashMap<>();

    /**
     * Constructor.
     *
     * @param nodeID the id of the node for this finger table
     */
    FingerTable(Integer nodeID, int numPeers) {
        this.nodeID = nodeID;
        this.table_size = calculateTableSize(numPeers);
        this.numPeers = numPeers;
        initTable();
    }

    /**
     * Given the number of peers in the network
     * calculates the size of the finger table.
     *
     * @param numNodes the number of nodes in the network
     * @return the finger table size.
     */
    private int calculateTableSize(int numNodes) {
        /* n = log_2(N) */
        return (int) (Math.round(Math.log(numNodes) / Math.log(2)));
    }

    /**
     * Initialize the table with the given
     * number of entries.
     */
    private void initTable() {
        for (int i = 0; i < table_size; i++) {
            table.put(i, null);
        }
    }

    /**
     * get the size of table
     *
     * @return the size of this table
     */
    int getTableSize() {
        return table_size;
    }

    /**
     * Returns the expected node at a given
     * index of the finger table if all nodes
     * are online.
     *
     * @param idx the finger table index.
     * @return the expected nodeID
     */
    int getExpectedNodeID(int idx) {
        /* j + 2^i % N*/
        return (int) (nodeID + Math.pow(2, idx)) % numPeers;
    }

    /**
     * Returns the actual id of the node stored
     * at the given index in the finger table.
     *
     * @param idx the table index
     * @return the node id stored.
     */
    NodeID getActualNode(int idx) {
        if (table.size() > idx) {
            return table.get(idx);
        } else
            printIndexError(idx);
        return null;
    }

    /**
     * Updates the nodeID in the finger table for the
     * given index.
     *
     * @param idx       the finger table index
     * @param newNodeID the nodeID to insert
     */
    void updateActualNodeID(int idx, NodeID newNodeID) {
        if (table.size() > idx) {
            table.put(idx, newNodeID);
        } else
            printIndexError(idx);
    }

    /**
     * Prints an error when attempting to
     * access non-existent index.
     *
     * @param i the index
     */
    private void printIndexError(int i) {
        System.out.println("Attempting to access non-existent row in finger table {" + i + "}.");
    }


    /**
     * Prints the finger table to stdout.
     */
    void printTable() {
        System.out.println("FINGER TABLE FOR NODE(" + nodeID + ")");
        System.out.println("i\tj+2^i\tactual");
        System.out.println("-------------------------");
        for (int i = 0; i < table.size(); i++) {
            NodeID node = table.get(i);
            System.out.println(String.format("%d%6d%8d", i, getExpectedNodeID(i), node.getId()));
        }
    }
}
