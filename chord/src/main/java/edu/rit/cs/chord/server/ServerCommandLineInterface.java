package edu.rit.cs.chord.server;

import edu.rit.cs.chord.CommandLineInterface;

class ServerCommandLineInterface extends CommandLineInterface {

    /**
     * Constructor.
     *
     * @param server the server class that stores node information.
     */
    ServerCommandLineInterface(ChordServer server) {
        addCommand("statuses", 's', "Print the list of nodes and current statuses.",
                (args) -> server.printTable());

        print_command_line_usage();
    }

}
