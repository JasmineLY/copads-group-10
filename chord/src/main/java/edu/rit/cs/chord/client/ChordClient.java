package edu.rit.cs.chord.client;

import edu.rit.cs.chord.NodeID;
import edu.rit.cs.chord.peer.Peer;
import edu.rit.cs.chord.peer.PeerInterface;
import edu.rit.cs.chord.server.ChordServer;
import edu.rit.cs.chord.server.ServerInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;

/**
 * Represents a client connecting to a DHT Chord System.
 */
public class ChordClient {

    private void startClient(String serverIP) {
        ClientConfig config = new ClientConfig();
        /* Get the port from the config file */
        int serverPort = config.getIntegerConfig(ClientConfig.SERVER_PORT_KEY);
        System.out.println("===============================================");
        System.out.println("Welcome to the Chord DHT System");
        try {
            /* Read from stdin */
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String line;

            /* Create object to process command line input. */
            ClientCommandLineInterface commandLine
                    = new ClientCommandLineInterface(
                    new ClientFileManager(config,
                            () -> getAnchorNode(serverIP, serverPort))
            );

            /* Start reading command line input */
            while ((line = reader.readLine()) != null) {
                boolean exit = commandLine.processLine(line);
                if (exit) break;
            }

        } catch (IOException e) {
            System.out.println("IO Error:" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Queries the server for an anchor node.
     *
     * @param serverIP   the server IP
     * @param serverPort the server port
     * @return an anchor node if one exists.
     */
    private PeerInterface getAnchorNode(String serverIP, int serverPort) {
        ServerInterface server = ChordServer.getServerStub(serverIP, serverPort);
        try {
            /* Connect to that anchor node */
            NodeID anchorID = server.getAnchorNode();
            if (anchorID == null) {
                System.out.println("Unable to connect: No anchor nodes are online.");
                System.exit(0);
            }

            return Peer.getPeerStub(anchorID);
        } catch (RemoteException e) {
            System.out.println("Failed to connect to DHT System: " + e.getMessage());
        }
        return null;
    }

    /**
     * Prints a description of how the program
     * should be run.
     */
    private static void print_usage() {
        System.out.println("Usage:");
        System.out.println("java [OPTIONS] edu.rit.cs.chord.client.ChordClient SERVER_IP");
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            print_usage();
            return;
        }

        String serverIP = args[0];

        new ChordClient().startClient(serverIP);
    }
}
