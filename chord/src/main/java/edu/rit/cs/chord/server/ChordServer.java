package edu.rit.cs.chord.server;

import edu.rit.cs.chord.CommandLineInterface;
import edu.rit.cs.chord.NodeID;
import edu.rit.cs.chord.NodeStatus;
import edu.rit.cs.chord.peer.Peer;
import edu.rit.cs.chord.peer.PeerInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import static edu.rit.cs.chord.NodeStatus.ONLINE;

public class ChordServer implements ServerInterface {

    private static final String NAME = "ChordServer";

    private Map<Integer, NodeID> onlineNodes = new HashMap<>();

    /**
     * Constructor
     */
    private ChordServer() {
    }

    private void startServer() {
        System.out.println("Server started.");
        try {
            /* Read from stdin */
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String line;

            CommandLineInterface commandLine = new ServerCommandLineInterface(this);

            /* Start reading command line input */
            while ((line = reader.readLine()) != null) {
                boolean exit = commandLine.processLine(line);
                if (exit) break;
            }
        } catch (IOException e) {
            System.out.println("IO Error:" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* Leave the Chord DHT System */
        System.exit(0);
    }

    /**
     * Attempts to add the new node to the system.
     * broadcast to all online nodes this node
     * has come online
     *
     * @param nodeID the nodeID for the new node
     * @return true if join was successful, false if
     * node already exists.
     */
    public boolean joinSystem(NodeID nodeID) {
        System.out.println("Node join: " + nodeID);
        /* If an ONLINE node with that ID already exists reject it. */
        if (onlineNodes.containsKey(nodeID.getId()))
            return false;
        else {
            /* Set the node and online status */
            onlineNodes.put(nodeID.getId(), nodeID);
            /* Tell all online nodes about the new node */
            broadcast(nodeID, ONLINE);
            return true;
        }
    }

    /**
     * Sets the given node status as offline in
     * the local table and notifies all online nodes
     * that the node has gone offline.
     *
     * @param nodeID the node leaving the system.
     */
    public void leaveSystem(NodeID nodeID) {
        System.out.println("Node leave: " + nodeID);
        if (onlineNodes.containsKey(nodeID.getId())) {
            /* Change the node status in the table */
            onlineNodes.remove(nodeID.getId());
            /* Tell all nodes that the node has left the system */
            broadcast(nodeID, NodeStatus.OFFLINE);
        } else
            System.out.println(nodeID + " is attempting to leave the system" +
                    " but has not registered with the server.");
    }

    /**
     * Broadcasts to all online nodes that the given
     * node has joined the system.
     *
     * @param eventNode the new node that has just joined.
     * @param newStatus whether this node come online or go offline
     */
    private void broadcast(NodeID eventNode, NodeStatus newStatus) {
        if (eventNode == null) {
            System.out.println("Attempting to update status " + newStatus + " for null node.");
            return;
        }

        onlineNodes.forEach((id, node) -> {
            PeerInterface peer = Peer.getPeerStub(node);
            if (peer != null) {
                try {
                    switch (newStatus) {
                        case ONLINE:
                            peer.notifyNodeOnline(eventNode);
                            break;
                        case OFFLINE:
                            peer.notifyNodeOffline(eventNode);
                            break;
                    }
                } catch (RemoteException e) {
                    System.out.println("Failed to notify " + node
                            + " of node " + eventNode
                            + " status update" + newStatus + " :" + e.getMessage());
                }
            } else {
                System.out.println("Failed to notify, peer with nodeID " + node + " is null.");
            }
        });
    }

    public NodeID getAnchorNode() {
        /* Return the first online node */
        for (NodeID node : this.onlineNodes.values())
                return node;
        return null;
    }

    /**
     * Returns the expectedID if it is online, otherwise
     * returns the next online node searching clockwise.
     * @param expectedID the expected id if all nodes online
     * @return the expected nodeID if it is online, otherwise
     * returns next online.
     */
    public NodeID nextOnlineClockwise(int expectedID) {
        return getNextOnlineNode(expectedID,  Math::min);
    }

    /**
     * Returns the expectedID if it is online, otherwise
     * returns the next online node searching counter-clockwise.
     * @param expectedID the expected id if all nodes online
     * @return the expected nodeID if it is online, otherwise
     * returns next online.
     */
    public NodeID nextOnlineCounterclockwise(int expectedID) {
        return getNextOnlineNode(expectedID, Math::max);
    }

    /**
     * Returns the expectedID if online, otherwise finds next online node.
     * @param expectedID    the id to start looking from
     * @param relationship  a function to handle node id comparison around the ring.
     * @return the next online node ID.
     */
    private NodeID getNextOnlineNode(int expectedID, BiFunction<Integer, Integer, Integer> relationship){
        if(onlineNodes.containsKey(expectedID))
            return onlineNodes.get(expectedID);
        else {
            Integer lessThan = null;
            Integer greaterThan = null;
            for (Integer id : onlineNodes.keySet()) {
                if(id < expectedID) {
                    if(lessThan == null) {
                        lessThan = id;
                        continue;
                    }
                    lessThan = relationship.apply(lessThan, id);
                } else {
                    if(greaterThan == null) {
                        greaterThan = id;
                        continue;
                    }
                    greaterThan = relationship.apply(greaterThan, id);
                }
            }

            Integer nextOnline;
            if(greaterThan != null)
                nextOnline = greaterThan;
            else nextOnline = lessThan;

            if(nextOnline != null)
                return onlineNodes.get(nextOnline);
            else return null;
        }
    }

    /**
     * Prints the current table of
     * node statuses.
     */
    void printTable() {
        System.out.println("NODE_ID\t\tSTATUS");
        System.out.println("--------------------");
        if (!onlineNodes.isEmpty())
            onlineNodes.forEach((id, nodeID) -> {
                System.out.println("\t" + id + "\t\t" + nodeID);
            });
        else
            System.out.println("No registered nodes.");
    }

    /**
     * Handles looking up the server stub from
     * the registry.
     *
     * @return the server stub if one is found.
     */
    public static ServerInterface getServerStub(String serverIP, int serverPort) {
        try {
            Registry registry = LocateRegistry.getRegistry(serverIP, serverPort);
            return (ServerInterface) registry.lookup(ChordServer.NAME);
        } catch (RemoteException | NotBoundException e) {
            System.out.println("Failed to join system: " + e.getMessage());
            System.exit(0);
        }
        return null;
    }


    /**
     * Sets up the server object to be used as a remote stub
     * by another object.
     *
     * @param onPort the port to create the registry on.
     */
    private void setupRemoteStub(int onPort) {
        try {
            ServerInterface stub = (ServerInterface) UnicastRemoteObject.exportObject(this, onPort);

            // Bind in the registry
            Registry registry = LocateRegistry.createRegistry(onPort);
            registry.rebind(NAME, stub);
        } catch (RemoteException e) {
            System.out.println("Error: Failed to setup server remote interface: " + e.getMessage());
        }
    }

    /**
     * Prints a description of how the program
     * should be run.
     */
    private static void print_usage() {
        System.out.println("Usage:");
        System.out.println("java [OPTIONS] edu.rit.cs.chord.server.ChordServer [PORT]");
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            print_usage();
            return;
        }

        int portNumber = 0;
        try {
            portNumber = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            System.out.println("Failed to start server: command line argument PORT is not a valid integer.");
            System.exit(0);
        }

        ChordServer server = new ChordServer();
        server.setupRemoteStub(portNumber);

        // If all is successful start the server.
        server.startServer();
    }

}
