package edu.rit.cs.chord.peer;

import edu.rit.cs.chord.NodeID;
import edu.rit.cs.chord.NodeStatus;
import edu.rit.cs.chord.NodeType;
import edu.rit.cs.chord.server.ChordServer;
import edu.rit.cs.chord.server.ServerInterface;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representative of a single peer in a Chord DHT System.
 */
public class Peer implements PeerInterface {
    private int Id;
    private NodeID predecessorID;
    private NodeID successorID;

    private static final String NAME = "Peer";

    /* The port to listen on for remote requests from peers */
    private int localPort;

    private PeerFileManager fileManager;

    private FingerTableManager fingerTableManager;

    private PeerConfig peerConfig;

    private ServerInterface server;

    /**
     * Constructor.
     *
     * @param localPort the local port to listen on for incoming RMI messages.
     */
    private Peer(int localPort) {
        this.localPort = localPort;
        this.peerConfig = new PeerConfig();
        this.fileManager = new PeerFileManager(this, peerConfig);
    }

    private void startPeer(String serverIP) {
        /* Get the server port */
        Integer serverPort = peerConfig.getIntegerConfig(PeerConfig.SERVER_PORT_KEY);
        if (serverPort == null) {
            System.out.println("Failed to connect to server: Invalid server port not specified.");
            System.exit(0);
        }

        try {
            /* Read from stdin */
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String line;

            /* Get the size of the ring */
            Integer ring_size = peerConfig.getIntegerConfig(PeerConfig.N_KEY);
            if (ring_size == null)
                ring_size = 16; // Default to ring size 16

            /* Keep attempting to join the system until success or exit. */
            boolean success = joinLoop(serverIP, serverPort, ring_size);
            if (!success)
                System.exit(0);

            /* Create object to process command line input. */
            PeerCommandLineInterface commandLine = new PeerCommandLineInterface(this,
                    this.fileManager, this.fingerTableManager);

            /* Start reading command line input */
            while ((line = reader.readLine()) != null) {
                boolean exit = commandLine.processLine(line);
                if (exit) break;
            }

        } catch (IOException e) {
            System.out.println("IO Error:" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* Leave the Chord DHT System */
        leaveSystem(serverIP, serverPort);
        System.exit(0);
    }

    @Override
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    private NodeID getPredecessorId() {
        return predecessorID;
    }

    void setPredecessorId(NodeID predecessorId) {
        this.predecessorID = predecessorId;
    }

    /**
     * the nodeID after my nodeID
     *
     * @return the next nodeID
     */
    private NodeID getSuccessorId() {
        return successorID;
    }

    void setSuccessorId(NodeID successorId) {
        this.successorID = successorId;
    }

    /**
     * Loops asking the user for a node id and validating
     * with the server until either a successful join or the
     * user decides to exit.
     *
     * @param serverIP   the server IP to connect to
     * @param serverPort the server port to connect on
     * @param ring_size  the size of the ring to restrict id size
     * @return true if successful join, false if the user wishes to exit
     * the program.
     */
    private boolean joinLoop(String serverIP, int serverPort, int ring_size) {
        /* Keep attempting to join the system until success or exit. */
        boolean successfulJoin = false;
        do {
            /* Ask the user to enter an id */
            this.Id = PeerCommandLineInterface.promptForId(ring_size);
            /* -1 is an invalid id, exit */
            if (Id == -1)
                return false;
            else
                this.fingerTableManager = new FingerTableManager(Id, ring_size, this);

            /* Attempt to join the Chord DHT System */
            successfulJoin = joinSystem(serverIP, serverPort);
        } while (!successfulJoin);
        System.out.println("\n\nWelcome to the Chord DHT System.");
        return true;
    }

    /**
     * Attempts to register with the ChordServer
     * and constructs the finger table if successful.
     *
     * @param serverIP   The ip of the server to connect to
     * @param serverPort The port to access on the server
     * @return true if successful join, false otherwise.
     */
    private boolean joinSystem(String serverIP, int serverPort) {
        this.server = ChordServer.getServerStub(serverIP, serverPort);
        try {
            /* Tell the server we are online */
            NodeID myID = generateNodeID();
            if (server.joinSystem(myID))
                return true;
            else
                System.out.println("Failed to join system: NodeID {" + this.Id + "} is already in use.");
        } catch (RemoteException e) {
            System.out.println("Failed to join system: " + e.getMessage());
        }
        /* Failed to join system node id not valid */
        return false;
    }

    /**
     * Generates the NodeID for the current
     * node.
     *
     * @return a nodeID object.
     */
    private NodeID generateNodeID() {
        String localIPAddr;
        try {
            /* Get the current IP address */
            localIPAddr = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            System.out.println("Failed to get local host ip: " + e.getMessage());
            localIPAddr = "localhost";
        }

        return new NodeID(this.Id, localIPAddr, this.localPort);
    }

    /**
     * Gets the peer stub for the given nodeID.
     *
     * @param nodeID the node id to locate the peer.
     * @return the remote stub peer object.
     */
    public static PeerInterface getPeerStub(NodeID nodeID) {
        try {
            Registry registry = LocateRegistry.getRegistry(nodeID.getIPAddr(), nodeID.getPort());
            return (PeerInterface) registry.lookup(Peer.NAME);
        } catch (RemoteException | NotBoundException e) {
            System.out.println("Failed to get peer: " + e.getMessage());
        }
        return null;
    }

    /**
     * When a node is going offline, transfer all files to
     * appropriate nodes then tell the server we are offline.
     *
     * @param serverIP   the server IP address
     * @param serverPort the server port to access
     */
    private void leaveSystem(String serverIP, int serverPort) {
        ServerInterface server = ChordServer.getServerStub(serverIP, serverPort);
        try {
            /* Distribute files amongst online nodes */
            distributeFiles();
            NodeID myID = generateNodeID();
            /* Tell the server we are offline */
            server.leaveSystem(myID);
        } catch (RemoteException e) {
            System.out.println("Failed to leave system: " + e.getMessage());
        }
    }

    /**
     * ask the server for the node wanted
     *
     * @param expectedID the expectID to look
     * @return the id that server find in it's map
     * for finger table to update
     * and for peer to update successor and predecessor
     */
    NodeID askServer(int expectedID, NodeType type) {
        try {
            switch (type){
                case ACTUAL: return this.server.nextOnlineClockwise(expectedID);
                case SUCCESSOR: return this.server.nextOnlineClockwise(fingerTableManager.inRing(expectedID + 1));
                case PREDECESSOR: return this.server.nextOnlineCounterclockwise(fingerTableManager.inRing(expectedID -1));
            }
        } catch (RemoteException e) {
            System.out.println("Failed to get the nodeID: " + e.getMessage());
        }
        return null;
    }

    @Override
    public List<String> getAllFilenames(List<Integer> visited) {
        if (visited.contains(this.getSuccessorId().getId())) {
            return new ArrayList<>(this.fileManager.getLocalFilenames());
        } else {
            PeerInterface successor = Peer.getPeerStub(this.getSuccessorId());
            if (successor != null) {
                try {
                    visited.add(this.getId());
                    /* Ask the next peer for their filenames and append to ours */
                    List<String> allFilenames = successor.getAllFilenames(visited);
                    allFilenames.addAll(this.fileManager.getLocalFilenames());
                    return allFilenames;
                } catch (RemoteException e) {
                    System.out.println("Failed to query all filenames: " + e.getMessage());
                }
            }
        }
        return null;
    }

    /**
     * When going offline distribute locally stored
     * files amongst all remaining online nodes.
     */
    private void distributeFiles() {
        List<File> localFiles = fileManager.getLocalFiles();
        if (localFiles.isEmpty())
            return;

        if (this.successorID.getId() != this.getId()) {
            PeerInterface nextPeer = getPeerStub(this.successorID);
            if (nextPeer != null) {
                try {
                    nextPeer.sendFiles(localFiles);
                } catch (RemoteException e) {
                    System.out.println("Failed to transfer files: " + e.getMessage());
                }
            } else {
                System.out.println("Failed to transfer files: unable to contact peer.");
            }
        } else {
            System.out.println("Failed to transfer files: no other nodes online.");
        }
    }

    /**
     * get the file from correct peer and return the file found
     *
     * @param hashCode the hash of the file to lookup
     * @return the file we get from the peer
     */
    @Override
    public File lookup(int hashCode, List<Integer> visited) {
        return nextHop(hashCode, nextPeer -> {
            /*
            If we have passed the goal or we have already visited this node
            accept it.
             */
            if (this.getId() >= fingerTableManager.expectedNode(hashCode) |
                    visited.contains(this.getId()))
                return this.fileManager.getFile(hashCode);
            else {
                visited.add(this.getId());
                return nextPeer.lookup(hashCode, visited);
            }
        });
    }

    /**
     * Looks up the NodeID for the peer
     * where the given file hash would be
     * stored.
     *
     * @param hashCode the hash of the filename
     * @return the NodeID of the peer where it is
     * stored.
     */
    @Override
    public NodeID lookupPeer(int hashCode, List<Integer> visited) {
        return nextHop(hashCode, nextPeer -> {
            if (this.getId() >= fingerTableManager.expectedNode(hashCode) |
                    visited.contains(this.getId()))
                return this.generateNodeID();
            else {
                visited.add(this.getId());
                return nextPeer.lookupPeer(hashCode, visited);
            }
        });
    }

    /**
     * to store the file onto the system
     * calling the fileManager's insert function
     *
     * @param downloadFile the file to download onto the system
     */
    @Override
    public boolean insert(File downloadFile) {
        int hashCode = downloadFile.getName().hashCode();
        NodeID peerID = lookupPeer(hashCode, new ArrayList<>());
        if(peerID.getId() == this.Id) {
            return this.fileManager.storeFile(downloadFile);
        } else {
            PeerInterface peer = Peer.getPeerStub(peerID);
            if (peer != null) {
                try {
                    return peer.insert(downloadFile);
                } catch (RemoteException e) {
                    System.out.println("Failed to insert file: " + e.getMessage());
                }
            } else
                System.out.println("Failed to insert file: cannot contact Node {" + peerID.getId() + "}");
        }
        return false;
    }


    /**
     * Handles recursively searching the DHT file system
     * by looking up the next hop in the finger table.
     *
     * @param hashCode the file hashcode to lookup the next peer
     * @param consumer the consumer function to perform function on next peer
     * @param <R>      the return type
     * @return an object of the specified return type.
     */
    private <R> R nextHop(int hashCode, RemoteGetter<PeerInterface, R> consumer) {
        /* Get the expected node id */
        int fileLocation = this.fingerTableManager.expectedNode(hashCode);
        try {
            /* If we are the current ID don't do lookup */
            if (fileLocation == this.getId()) {
                return consumer.get(this);
            } else {
                /* Get the actual node id */
                NodeID nodeID = this.fingerTableManager.nextNode(fileLocation);
                if (nodeID == null)
                    return null;
                /* Otherwise recurse to next hop */
                PeerInterface nextPeer = Peer.getPeerStub(nodeID);
                if (nextPeer != null)
                    return consumer.get(nextPeer);
            }
        } catch (RemoteException e) {
            System.out.println("Failed to perform next hop: " + e.getMessage());
        }
        return null;
    }

    /**
     * send files from previous peer to current peer
     *
     * @param files the files that were stored on previous peer
     */
    @Override
    public void sendFiles(List<File> files) {
        System.out.println("Received {" + files.size() + "} files from peer.");
        files.forEach(file -> {
            this.fileManager.storeFile(file);
        });
    }

    @Override
    public void notifyNodeOnline(NodeID nodeID) {
        this.fingerTableManager.updateFingerTable(nodeID);
        /* If a predecessor came online we may need to move some files */
        if (this.getPredecessorId().equals(nodeID)) {
            /* Do we have files to send? */
            List<File> peerFiles = this.fileManager.getFiles(nodeID.getId());
            if(!peerFiles.isEmpty()) {
                /* Yes, transfer the files */
                PeerInterface predecessor = Peer.getPeerStub(this.getPredecessorId());
                if (predecessor != null) {
                    try {
                        predecessor.sendFiles(peerFiles);
                        /* If send was successful delete the files from current node */
                        this.fileManager.delete(peerFiles);
                    } catch (RemoteException e) {
                        System.out.println("Failed to send files to \'"
                                + nodeID.getId() + "\': " + e.getMessage());
                    }
                } else {
                    System.out.println("Failed to send files: unable to contact predecessor.");
                }
            }
        }
    }

    @Override
    public void notifyNodeOffline(NodeID nodeID) {
        this.fingerTableManager.removeNodeFromTable(nodeID);
    }

    /**
     * Exports the peer object to the remote registry so that it can
     * be looked up and used by other nodes.
     *
     * @param onPort the port to create the registry on.
     */
    private void setupRemoteStub(int onPort) {
        try {
            PeerInterface stub = (PeerInterface) UnicastRemoteObject.exportObject(this, 0);

            // Bind in the registry
            Registry registry = LocateRegistry.createRegistry(onPort);
            registry.rebind(NAME, stub);
        } catch (RemoteException e) {
            System.out.println("Error: Failed to setup peer remote interface: " + e.getMessage());
        }
    }

    /**
     * Prints a description of how the program
     * should be run.
     */
    private static void print_usage() {
        System.out.println("Usage:");
        System.out.println("java [OPTIONS] edu.rit.cs.chord.peer.Peer SERVER_IP PEER_PORT");
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            print_usage();
            return;
        }

        String serverIP = args[0];
        int localPort = Integer.parseInt(args[1]);

        /* Setup */
        Peer peer = new Peer(localPort);
        peer.setupRemoteStub(localPort);

        /* If everything is successful start the peer */
        peer.startPeer(serverIP);
    }

    /**
     * Allows for consumer to throw RemoteException
     *
     * @param <T>
     */
    private interface RemoteConsumer<T> {
        void apply(T t) throws RemoteException;
    }

    /**
     * Allows for consumer to throw RemoteException
     * and return an object.
     *
     * @param <T> the object to take in
     * @param <R> the object to return
     */
    private interface RemoteGetter<T, R> {
        R get(T t) throws RemoteException;
    }
}
