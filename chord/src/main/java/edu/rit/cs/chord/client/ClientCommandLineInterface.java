package edu.rit.cs.chord.client;

import edu.rit.cs.chord.CommandLineInterface;
import edu.rit.cs.chord.FileManager;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 * Defines the commands that the user can enter when interacting
 * with the client.
 */
class ClientCommandLineInterface extends CommandLineInterface {

    ClientCommandLineInterface(FileManager fileManager) {
        addCommand("upload", 'u', "Upload a file to the file system.",
                (args) -> uploadFile(fileManager));
        addCommand("download", 'd', "Download a file from the file system.",
                (args) -> downloadFile(fileManager));
        addCommand("files", 'f', "List all files in the system.",
                (args) -> listAllFiles(fileManager.getAllFilenames()));

        print_command_line_usage();
    }

    /**
     * Prompts the user for a filename.
     *
     * @param in the scanner reading from stdin
     * @return
     */
    private String getFilename(Scanner in) {
        System.out.print("Enter a filename (press ENTER to exit): ");
        String choice = in.nextLine().trim();
        if (choice.isEmpty())
            return null;
        else
            return choice;
    }

    /**
     * Asks the user for a filename,
     * otherwise creates a file with the
     * given name and content and adds it
     * to the system.
     */
    private void uploadFile(FileManager fileManager) {
        Scanner in = new Scanner(System.in);
        boolean leave = false;
        while (!leave) {
            System.out.println("===========================================================");
            System.out.println("Upload a file:");
            System.out.println("1: Upload an existing file");
            System.out.println("2: Create a new file");
            System.out.print("Make a selection [press ENTER to exit]: ");
            String choice = in.nextLine().trim();
            switch (choice) {
                case "1":
                    String filename;
                    if ((filename = getFilename(in)) != null) {
                        String filePath = FileManager.generateFilePath(
                                fileManager.getUploadFileDirectory(), filename);
                        if (promptForValidation(in, filePath)) {
                            /* Send the file to the peer to store */
                            fileManager.uploadFile(new File(filePath));
                        } else {
                            System.out.println("Exiting file upload.");
                        }
                    }
                    break;
                case "2":
                    String content;
                    if (!(content = getFileContent(in)).isEmpty()) {
                        String newFilename;
                        if ((newFilename = getFilename(in)) != null) {
                            String filepath = FileManager.generateFilePath(fileManager.getUploadFileDirectory(), newFilename);
                            if (promptForValidation(in, filepath)) {
                                File f = generateFile(filepath, content);
                                /* Send the file to the peer to store */
                                fileManager.uploadFile(f);
                            } else {
                                System.out.println("Discarding file.");
                            }
                        }
                    } else {
                        System.out.println("Cannot store empty file.");
                    }
                    break;
                case "":
                    leave = true;
                    /* Navigate back up to main command line */
                    print_command_line_usage();
                    break;
                default:
                    System.out.println("Invalid choice \'" + choice + "\'");
                    break;
            }
        }
    }

    /**
     * Asks the user for the filename
     * and searches the system for the given file
     * to download.
     *
     * @param fileManager The manager to handle downloading the file
     */
    private void downloadFile(FileManager fileManager) {
        System.out.println("===========================================================");
        System.out.println("Download a file:");
        listAllFiles(fileManager.getAllFilenames());
        Scanner in = new Scanner(System.in);
        String filename;
        if ((filename = getFilename(in)) != null) {
            /* Ask the peer to download the file */
            fileManager.downloadFile(filename);
        } else {
            System.out.println("Exiting file download.");
        }
        print_command_line_usage();
    }

    /**
     * Prompts the user to create a new file.
     *
     * @param in the scanner reading from stdin
     * @return the new file content.
     */
    private String getFileContent(Scanner in) {
        StringBuilder builder = new StringBuilder();
        System.out.println("Enter file content (enter an empty line when done): ");
        String line = in.nextLine().trim();
        /* Get each line of content */
        while (!line.isEmpty()) {
            builder.append(line);
            builder.append("\n");
            line = in.nextLine().trim();
        }

        return builder.toString();
    }

    /**
     * Generates a new file from the given filename
     * and content.
     *
     * @param filepath the filepath for the new file
     * @param content  the content of the new file
     * @return the file object, null if error.
     */
    private File generateFile(String filepath, String content) {
        File f = new File(filepath);

        if (!f.exists()) {
            try {
                if (!f.createNewFile()) {
                    System.out.println("Failed to create file \'" + filepath + "\'");
                    return null;
                }
            } catch (IOException e) {
                System.out.println("Error creating file: " + e.getMessage());
                return null;
            }
        }

        /* Write to file */
        try {
            FileWriter fileWriter = new FileWriter(f);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error writing to new file \'" + filepath + "\'.");
            return null;
        }

        /* Prevent need to keep deleting files with every test */
        f.deleteOnExit();
        return f;
    }

    /**
     * Prompts the user to validate the given file and content.
     *
     * @param in       the scanner reading from stdin
     * @param filename the filename
     * @return true if the wishes to upload, false otherwise.
     */
    private boolean promptForValidation(Scanner in, String filename) {
        System.out.println("File: " + filename);
        System.out.println("=========================================================");
        while (true) {
            System.out.print("Is this the file you would like to upload? [y/n]: ");
            String choice = in.nextLine().trim();
            if (choice.equalsIgnoreCase("Y") | choice.equalsIgnoreCase("Yes")) {
                return true;
            } else if (choice.equalsIgnoreCase("N") | choice.equalsIgnoreCase("No")) {
                return false;
            }
        }
    }

    /**
     * Lists all the files stored on the current system.
     *
     * @param filenames the list of all filenames.
     */
    private void listAllFiles(List<String> filenames) {
        int size = filenames.size();
        System.out.println("Chord DHT File System: {" + size + "} file(s) stored:");
        if (size > 0)
            filenames.forEach((filename) -> System.out.println("\t" + filename));
        else
            System.out.println("\tNo files.");
    }
}
