package edu.rit.cs.chord;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Config {
    private Map<String, String> configurations;

    public Config(String configFilePath) {
        this.configurations = parseConfigFile(configFilePath);
    }

    /**
     * Returns a configuration value for the given key
     * as an integer.
     *
     * @param configKey the config key
     * @return the value if one was found and was valid, null
     * if error.
     */
    public Integer getIntegerConfig(String configKey) {
        String value = getStringConfig(configKey);
        if (value != null) {
            try {
                Integer intValue = Integer.valueOf(value);
                return intValue;
            } catch (NumberFormatException e) {
                System.out.println("Failed to convert config value: \'" + value + "\' for key \'" + configKey + "\' is not an integer.");
            }
        }
        return null;
    }

    /**
     * Returns a configuration value for the given key
     * as a String.
     *
     * @param configKey the config key
     * @return the string value if found, null otherwise.
     */
    public String getStringConfig(String configKey) {
        String value = configurations.get(configKey);
        if (value == null) {
            System.out.println("No config value exists for key \'" + configKey + "\'");
            return null;
        }
        return value;
    }

    /**
     * Parses the configuration file and returns a mapping
     * of configuration keys to values.
     *
     * @return a map of config keys to values.
     */
    private Map<String, String> parseConfigFile(String configFilePath) {
        Map<String, String> configurations = new HashMap<>();
        System.out.println("Config file path: " + configFilePath);
        File configFile = new File(configFilePath);
        if (!configFile.exists())
            System.out.println("Error setting up node: Missing configuration file.");
        else {
            try {
                BufferedReader br = new BufferedReader(new FileReader(configFile));

                String line;
                while ((line = br.readLine()) != null) {
                    if (!line.isEmpty()) {
                        /* Get everything before the comment '#' */
                        String newLine = line.split("#")[0];
                        if (newLine.isEmpty())
                            continue;

                        /* Split on equals '=' */
                        String[] config = newLine.split("=");

                        /* Check that the file was split properly */
                        if (config.length == 1) {
                            System.out.println("Error parsing config file: Invalid line \'" + line + "\'");
                            break;
                        } else
                            configurations.put(config[0], config[1]);
                    }
                }

                br.close();
            } catch (IOException e) {
                System.out.println("Error setting up node: " + e.getMessage());
            }
        }

        return configurations;
    }
}
