package edu.rit.cs.chord.peer;

import edu.rit.cs.chord.NodeID;
import edu.rit.cs.chord.NodeStatus;
import edu.rit.cs.chord.NodeType;
import org.w3c.dom.Node;

/**
 * Class that handles updating the
 * Finger Table when nodes go online/offline.
 */
class FingerTableManager {

    private Integer nodeID;
    private FingerTable table;
    private Peer peer;
    private int numPeers;

    /**
     * Constructor.
     *
     * @param nodeID   the id of the current node
     * @param numPeers the number of nodes in the system.
     */
    FingerTableManager(Integer nodeID, int numPeers, Peer peer) {
        this.nodeID = nodeID;
        this.table = new FingerTable(nodeID, numPeers);
        this.peer = peer;
        this.numPeers = numPeers;
    }

    int getNodeID() {
        return this.nodeID;
    }

    /**
     * Returns the next nodeID
     * to know which peer to go
     * look through the fingerTable and do hops
     *
     * @param nodeID the expected nodeID
     * @return the actual nodeID
     */
    NodeID nextNode(int nodeID) {
        for (int idx = 0; idx < table.getTableSize(); idx++) {
            if (table.getExpectedNodeID(idx) == nodeID) {
                return table.getActualNode(idx);
            } else if (table.getExpectedNodeID(idx) > nodeID) {
                return table.getActualNode(Math.max(idx - 1, 0));
            }
        }
        return table.getActualNode((table.getTableSize() - 1));
    }

    /**
     * Returns the node id of node
     * where the file is expected to
     * be stored in a scenario where
     * all nodes are online.
     *
     * @param fileHash the filename after it has been put
     *                 through the hash function.
     * @return the expected node id in the range of 0-N.
     */
    int expectedNode(int fileHash) {
        return Math.floorMod(fileHash, this.numPeers);
    }

    /**
     * update the finger table to correct nodes
     * ask the server for the node
     * onlineNodes stored on the server is up to date.
     */
    void updateFingerTable(NodeID updateID) {
        /* Loop through finger table */
        for (int idx = 0; idx < table.getTableSize(); idx++) {
            int expectID = table.getExpectedNodeID(idx);
            NodeID actual = table.getActualNode(idx);
            if(actual == null)
                /* First join, ask the server to populate finger table */
                table.updateActualNodeID(idx, peer.askServer(expectID, NodeType.ACTUAL));
            else if(expectID == updateID.getId())
                /* If values match just set the actual */
                table.updateActualNodeID(idx, updateID);
            else {
                /* Update the index to min(actual, updateID) */
                table.updateActualNodeID(idx, calculateSuccessor(expectID, actual, updateID));
            }
        }
        /* Get successor from table */
        peer.setSuccessorId(table.getActualNode(0));
        /* Only need to ask server for predecessor */
        peer.setPredecessorId(peer.askServer(this.nodeID, NodeType.PREDECESSOR));
    }

    /**
     * Removes the given node from the table in the
     * case of going offline.
     * @param nodeID the offline node.
     */
    void removeNodeFromTable(NodeID nodeID) {
        /* Loop through finger table */
        for (int idx = 0; idx < table.getTableSize(); idx++) {
            int expectID = table.getExpectedNodeID(idx);
            NodeID actual = table.getActualNode(idx);
            if(actual.equals(nodeID))
                /* Ask the server for the new node. */
                table.updateActualNodeID(idx, peer.askServer(expectID, NodeType.ACTUAL));
        }
        /* Get successor from table */
        peer.setSuccessorId(table.getActualNode(0));
        /* Only need to ask server for predecessor */
        peer.setPredecessorId(peer.askServer(this.nodeID, NodeType.PREDECESSOR));
    }

    /**
     * Converts the given id to make sure it is still
     * in the ring.
     * @param id the id to convert
     * @return a number between 0 - (N-1)
     */
    int inRing(int id){
        return Math.floorMod(id, numPeers);
    }

    /**
     * Determines which of the two nodeIDs is the successor
     * of the expected id and returns it.
     * @param expectedId the expected id
     * @param a the first nodeID
     * @param b the second nodeID
     * @return the successor NodeID
     */
    private NodeID calculateSuccessor(int expectedId, NodeID a, NodeID b){
        int a_unmod = a.getId();
        int b_unmod = b.getId();
        /* Change 'ring' back into number line to compare distance */
        if(a_unmod < expectedId)
            a_unmod += this.numPeers;
        if(b_unmod < expectedId)
            b_unmod += this.numPeers;

        /* Return the min */
        if(a_unmod < b_unmod)
            return a;
        else
            return b;
    }

    /**
     * Print the current finger table.
     */
    void printTable() {
        this.table.printTable();
    }
}
