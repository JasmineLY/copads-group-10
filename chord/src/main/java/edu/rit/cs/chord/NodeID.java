package edu.rit.cs.chord;

import java.io.Serializable;

/**
 * A lightweight class that represents
 * a node in the system as visible to
 * a peer.
 */
public class NodeID implements Serializable, Comparable<NodeID> {
    private Integer id;
    private String ip;
    private int port;

    public NodeID(Integer id, String ip, int port) {
        this.id = id;
        this.ip = ip;
        this.port = port;
    }

    public Integer getId() {
        return id;
    }

    public String getIPAddr() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "NodeID{id: " + id + ", ip: " + ip + ", port: " + port + "}";
    }

    @Override
    public int compareTo(NodeID o) {
        return this.getId().compareTo(o.getId());
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null || obj.getClass() != this.getClass())
            return false;

        NodeID node = (NodeID) obj;

        return node.id.equals(this.id);
    }
}
