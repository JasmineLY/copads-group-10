package edu.rit.cs.chord.peer;

import edu.rit.cs.chord.NodeID;

import java.io.File;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface PeerInterface extends Remote {
    int getId() throws RemoteException;

    NodeID lookupPeer(int hashCode, List<Integer> visited) throws RemoteException;

    File lookup(int hashCode, List<Integer> visited) throws RemoteException;

    List<String> getAllFilenames(List<Integer> visited) throws RemoteException;

    boolean insert(File file) throws RemoteException;

    void sendFiles(List<File> files) throws RemoteException;

    void notifyNodeOnline(NodeID nodeID) throws RemoteException;

    void notifyNodeOffline(NodeID nodeID) throws RemoteException;
}
