package edu.rit.cs.chord.client;

import edu.rit.cs.chord.FileManager;
import edu.rit.cs.chord.peer.PeerInterface;

import java.io.File;
import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.util.*;

public class ClientFileManager extends FileManager {

    private FileManagerInterface fileManagerInterface;

    private PeerInterface anchorNode;

    private int MAX_RETRY = 1; // Default to 1 retry

    /**
     * Constructor.
     *
     * @param config the configuration class
     */
    ClientFileManager(ClientConfig config, FileManagerInterface fileManagerInterface) {
        super(config);
        this.fileManagerInterface = fileManagerInterface;
        setDownloadDirectory(config.getStringConfig(ClientConfig.DOWNLOAD_DIR_KEY));
        setUploadDirectory(config.getStringConfig(ClientConfig.UPLOAD_DIR_KEY));
        this.anchorNode = fileManagerInterface.getAnchorNode();
        this.MAX_RETRY = config.getIntegerConfig(ClientConfig.RETRY_KEY);
    }

    /** Reset the anchor node by asking the server for a new node
     *  in the case that one went offline.
     */
    private void reset() {
        /* Get new anchor node and try again */
        this.anchorNode = fileManagerInterface.getAnchorNode();
    }

    @Override
    public void uploadFile(File file) {
        int tries = 0;
        do {
            if (anchorNode != null) {
                try {
                    if (anchorNode.insert(file))
                        System.out.println("Upload successful.");
                    else
                        System.out.println("Failed to upload file.");
                    return;
                } catch (ConnectException e) {
                    /* Get new anchor node and try again */
                    reset();
                } catch (RemoteException e) {
                    System.out.println("Failed to upload file: " + e.getMessage());
                }
            } else {
                reset();
            }
            tries++;
        } while(tries <= MAX_RETRY);
    }

    @Override
    public void downloadFile(String filename) {
        int tries = 0;
        do {
            if (anchorNode != null) {
                try {
                    File file = anchorNode.lookup(hashFilename(filename), new ArrayList<>());
                    if (file != null) {
                        storeFile(file);
                        System.out.println("Download successful.");
                    } else {
                        System.out.println("File \'" + filename + "\' does not exist in the system.");
                    }
                    return;
                } catch (ConnectException e) {
                    reset();
                } catch (RemoteException e) {
                    System.out.println("Failed to download file \'"
                            + filename + "\': " + e.getMessage());
                }
            } else {
                reset();
            }
            tries++;
        } while(tries <= MAX_RETRY);
    }

    @Override
    public List<String> getAllFilenames() {
        int tries = 0;
        do {
            if (anchorNode != null) {
                try {
                    Set<String> filenames = new HashSet<>(anchorNode.getAllFilenames(new ArrayList<>()));
                    List<String> files = new ArrayList<>(filenames);
                    Collections.sort(files);
                    return files;
                } catch (ConnectException e) {
                    reset();
                } catch (RemoteException e) {
                    System.out.println("Failed to query files on system: " + e.getMessage());
                }
            } else {
                reset();
            }
            tries ++;
        } while (tries <= MAX_RETRY);
        return new ArrayList<>();
    }

    interface FileManagerInterface {
        PeerInterface getAnchorNode();
    }
}
