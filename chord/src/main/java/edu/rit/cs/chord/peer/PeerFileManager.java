package edu.rit.cs.chord.peer;

import edu.rit.cs.chord.FileManager;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles managing the file system for a single
 * peer.
 *
 * @author Alexis Holler
 */
class PeerFileManager extends FileManager {

    private Peer peer;

    PeerFileManager(Peer peer, PeerConfig config) {
        super(config);
        this.peer = peer;
        setDownloadDirectory(config.getStringConfig(PeerConfig.DOWNLOAD_DIR_KEY));
    }

    /**
     * Returns the download directory to
     * download all files to.
     *
     * @return the directory path
     */
    @Override
    public String getDownloadFileDirectory() {
        String dir = DEFAULT_FILE_DIRECTORY;
        if (this.downloadDirectory != null)
            dir = this.downloadDirectory;
        return dir + "_" + peer.getId();
    }

    /**
     * Hashes the filename and sends it to the appropriate
     * peer node to store.
     *
     * @param file the file to download
     */
    public void uploadFile(File file) {
        if (!file.exists()) {
            System.out.println("Failed to upload: file \'" + file.getName() + "\' does not exist.");
            return;
        }
        this.peer.insert(file);
    }


    /**
     * Looks up the filename in the finger table
     * to locate the peer that is storing the file
     * and requests it.
     *
     * @param filename the name of the file to query.
     */
    public void downloadFile(String filename) {
        File downloadedFile = this.peer.lookup(hashFilename(filename), new ArrayList<>());
        if (peer == null) {
            System.out.println("Failed to download the file unable to find online peer.");
            return;
        }
        if (downloadedFile == null) {
            System.out.println("The file \'" + filename + "\' does not exist on the system.");
            return;
        }
        storeFile(downloadedFile);
    }

    /**
     * Get a local file given the filename.
     *
     * @param hashCode the hash of the filename.
     * @return the file if it exists.
     */
    File getFile(int hashCode) {
        for (File file : getLocalFiles()) {
            int fileHash = PeerFileManager.hashFilename(file.getName());
            if (fileHash == hashCode)
                return file;
        }
        return null;
    }

    /**
     * Get the files whose hash is the given hash or less
     *
     * @param nodeID the end hash range (inclusive)
     * @return the files on the current node that fall below the range
     */
    List<File> getFiles(int nodeID) {
        List<File> files = new ArrayList<>();
        for (File file : getLocalFiles()) {
            int fileHash = hashFilename(file.getName());
            if (fileHash <= nodeID)
                files.add(file);
        }
        return files;
    }

    /**
     * Deletes the given files from the
     * file directory.
     *
     * @param files the list of files to delete
     */
    void delete(List<File> files) {
        files.forEach(File::delete);
    }


    /**
     * Returns the name of all the files stored on all nodes.
     *
     * @return the list of filenames.
     */
    public List<String> getAllFilenames() {
        return this.peer.getAllFilenames(new ArrayList<>());
    }

}
