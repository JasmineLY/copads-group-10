package edu.rit.cs.chord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * Handles processing command line input.
 *
 * @author Alexis Holler
 */
public class CommandLineInterface {

    /* The list of defined commands */
    private List<Command> commandList = new ArrayList<>();

    /**
     * Constructor.
     */
    public CommandLineInterface() {
        /* Add and define commands */
        addExitCommand();
        addHelpCommand();
    }

    /**
     * Add a complex command that takes arguments.
     *
     * @param keyword      the keyword the user types
     * @param char_keyword a character shortcut of keyword
     * @param desc         a description of the command
     * @param argsUsage    an array that defines the usage of each argument
     * @param operation    the operation to perform when the user types the command
     */
    protected void addCommand(String keyword, char char_keyword, String desc,
                              String[] argsUsage, Consumer<String[]> operation) {
        commandList.add(new Command(keyword, char_keyword, desc, false, argsUsage, operation));
    }

    /**
     * Add simple command that takes no arguments.
     *
     * @param keyword      the keyword defining the command
     * @param char_keyword a character shortcut of the keyword
     * @param desc         a description of the command.
     * @param function     the operation to perform when the user types the command.
     */
    protected void addCommand(String keyword, char char_keyword, String desc, Consumer<String[]> function) {
        commandList.add(new Command(keyword, char_keyword, desc, false, new String[]{}, function));
    }


    /**
     * Shortcut function for exit command.
     */
    private void addExitCommand() {
        commandList.add(new Command("quit", 'q',
                "Exits the program", true, new String[]{}, null));
    }

    /**
     * Shortcut function for help command.
     */
    private void addHelpCommand() {
        addCommand("help", 'h', "Print the command list",
                s -> print_command_line_usage());
    }


    /**
     * Process a line of user input from the command line.
     *
     * @param line the user input
     * @return true if the program should exit, false otherwise
     */
    public boolean processLine(String line) {
        if (line.isEmpty()) return false;

        String[] line_array = line.split("\\s+");
        String input = line_array[0];

        for (Command c : commandList) {
            if (c.matches(input)) {
                String[] args = {};
                /* Get the arguments and pass it to the function */
                if (line_array.length > 1) args = Arrays.copyOfRange(line_array, 1, line_array.length);
                return c.perform(args);
            }
        }

        System.out.println("Unknown command: '" + line + "'");

        return false;
    }

    /**
     * Prints out the defined command line inputs when
     * the EventManager is running.
     */
    protected void print_command_line_usage() {
        System.out.println("===========================================================");
        System.out.println("Commands:\n");
        for (Command c : commandList) {
            System.out.println(c);
        }
        System.out.println("===========================================================");
    }

    /**
     * Class encapsulating a single command
     * in the command line.
     */
    private class Command {
        /* The command name */
        private String keyword;

        /* A short form for the command */
        private char char_keyword;

        /* The command description */
        private String desc;

        /* Should the command quit the program */
        private boolean exit;

        /* The function to execute when the command is called */
        private Consumer<String[]> function;

        /* The usage for each argument */
        private String[] argsUsage;

        /**
         * Constructor.
         *
         * @param keyword      the string keyword to define command.
         * @param char_keyword a character shorthand for keyword.
         * @param desc         the command description.
         * @param exit         true if the program should exit after executing
         * @param function     the function to execute when command is called.
         */
        private Command(String keyword, char char_keyword, String desc,
                        boolean exit, String[] argsUsage,
                        Consumer<String[]> function) {
            this.keyword = keyword;
            this.char_keyword = char_keyword;
            this.desc = desc;
            this.exit = exit;
            this.function = function;
            this.argsUsage = argsUsage;
        }

        /**
         * Determines if the line matches either of the
         * keywords.
         *
         * @param line the line to compare
         * @return true if it matches false otherwise
         */
        private boolean matches(String line) {
            return line.equalsIgnoreCase(keyword) | line.equalsIgnoreCase(String.valueOf(char_keyword));
        }

        /**
         * Perform the given command.
         *
         * @param args the arguments to pass to the function
         * @return true if the program should exit
         * false otherwise.
         */
        private boolean perform(String[] args) {
            if (function != null) {
                /* Check argument length */
                if (args.length != argsUsage.length) {
                    System.out.println("Invalid argument length.");
                    print_usage();
                    return false;
                }

                /* Call function */
                function.accept(args);
            }
            return exit;
        }

        /**
         * Prints the usage message for the command.
         */
        private void print_usage() {
            System.out.println("Usage:");
            StringBuilder argsString = new StringBuilder(keyword + "|" + char_keyword + " ");
            for (int i = 0; i < argsUsage.length; i++) {
                argsString.append(argsUsage[i]);
                argsString.append(" ");
            }
            System.out.println(argsString);
        }

        @Override
        public String toString() {
            String format = "%s, %-20s %5s";
            return String.format(format, char_keyword, keyword, desc);
        }
    }
}
