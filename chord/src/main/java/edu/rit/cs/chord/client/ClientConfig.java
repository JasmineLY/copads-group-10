package edu.rit.cs.chord.client;

import edu.rit.cs.chord.Config;

class ClientConfig extends Config {
    private static String CONFIG_FILE_PATH
            = System.getProperty("user.dir") + "\\src\\main\\java\\edu\\rit\\cs\\chord\\client\\client.cfg";


    /* Config keys */
    static final String SERVER_PORT_KEY = "server_port";
    static final String UPLOAD_DIR_KEY = "upload_directory";
    static final String DOWNLOAD_DIR_KEY = "download_directory";
    static final String RETRY_KEY = "max_retry";

    ClientConfig() {
        super(CONFIG_FILE_PATH);
    }
}
