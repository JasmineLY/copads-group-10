package edu.rit.cs.chord.peer;

import edu.rit.cs.chord.CommandLineInterface;
import edu.rit.cs.chord.NodeID;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The class that controls the command line interface when
 * running a peer.
 *
 * @author Alexis Holler
 */
class PeerCommandLineInterface extends CommandLineInterface {


    /**
     * Constructor.
     *
     * @param fileManager the manager that holds information about files.
     */
    PeerCommandLineInterface(Peer peer, PeerFileManager fileManager, FingerTableManager fingerTableManager) {

        addCommand("files", 'f', "List files stored on this node.",
                (args) -> listFiles(peer.getId(), fileManager.getLocalFilenames()));

        addCommand("location", 'l', "Return the nodeID where the given file should be stored.",
                new String[]{"FILENAME"}, (args) -> printFileHash(args[0], peer, fingerTableManager));

        addCommand("id", 'i', "Return the current node ID",
                (args) -> System.out.println("The current node id is: " + fingerTableManager.getNodeID()));

        addCommand("table", 't', "Print the current finger table.",
                (args) -> fingerTableManager.printTable());

        print_command_line_usage();
    }

    /**
     * Prompts the user to enter a node id between
     * 1 and the given ring size
     *
     * @param ring_size the max size for the DHT ring
     * @return the id if one was chosen, 0 if the user
     * chose to exit.
     */
    static int promptForId(int ring_size) {
        Scanner in = new Scanner(System.in);
        final int invalidID = -1;
        int id = invalidID;

        /* Loop while the id is invalid */
        while (id == invalidID) {
            /* Prompt the user for an id between 0-(n-1) */
            System.out.print("Enter a node id between 0-" + (ring_size - 1) + " [press ENTER to exit]: ");
            String line = in.nextLine();

            /* Exit if the user hits enter */
            if (line.isEmpty())
                return invalidID;

            /* Check that the id is valid */
            try {
                int entered_id = Integer.parseInt(line);
                if (entered_id > (ring_size - 1) | entered_id < 0) {
                    System.out.println("Invalid ID: Id out of range.");
                } else
                    id = entered_id;
            } catch (NumberFormatException e) {
                System.out.println("Invalid ID: \'" + line + "\' is not an integer.");
            }
        }
        return id;
    }

    /**
     * Prints the nodeID where the file should be located.
     *
     * @param filename           the filename to store
     * @param fingerTableManager the manager that holds node information.
     */
    private void printFileHash(String filename, Peer peer, FingerTableManager fingerTableManager) {
        System.out.println("File \'" + filename + "\' location:");
        int fileHash = PeerFileManager.hashFilename(filename);
        int expectedNode = fingerTableManager.expectedNode(fileHash);
        NodeID nodeID = peer.lookupPeer(fileHash, new ArrayList<>());
        System.out.println("\tExpected to be at node {" + expectedNode + "}");
        System.out.println("\tStored at next online node {" + nodeID.getId() + "}.");
    }

    /**
     * Prints the list of files stored
     * on the current node.
     */
    private void listFiles(int nodeID, List<String> filenames) {
        System.out.println("{" + filenames.size() + "} file(s) stored on Node " + nodeID + ":");
        if (!filenames.isEmpty()) {
            filenames.forEach((filename) -> System.out.println("\t" + filename));
        } else
            System.out.println("\tNo files.");
    }

}
