###To build project
```
mvn package
```

### How to run Chord DHT
```
cd chord
run ChordServer.
run Peer(s).
run Client.
when running ChordServer, enter port you are going to use for Peers to connect to
when running Peer, enter 'localhost' for HOST IP if manager also running on local,
                          use same port with ChordServer
```

### Run ChordServer
```
java -cp target/chord-1.0.jar edu.rit.cs.chord.server.ChordServer [PORT]
```

### Run Peer
* NODE PORT must be unique for every node
```
java -cp target/chord-1.0.jar edu.rit.cs.chord.peer.Peer [SERVER IP] [NODE PORT]
```

### Run Client
```
java -cp target/chord-1.0.jar edu.rit.cs.chord.client.ChordClient [SERVER IP]
```